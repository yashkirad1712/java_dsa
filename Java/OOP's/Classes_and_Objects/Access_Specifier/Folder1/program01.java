/*Acess Specifier : Default
 * Can be access in same file and same folder*/

class Core2Web{
	
	int numCourses = 8;
	private String FavCourse = "cpp";

	void Display(){
		
		System.out.println(numCourses);
		System.out.println(FavCourse);
	}
}


class Student{
	
	public static void main(String[] args){
		
		Core2Web obj = new Core2Web();
		obj.Display();

		System.out.println(obj.numCourses);
		//System.out.println(obj.FavCourse);  Error : FavCourse has private access in Core2Web
	}
}
