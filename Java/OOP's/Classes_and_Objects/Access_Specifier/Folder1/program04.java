class Employee{
	
	int empId = 10;
	String str = "Kanha";

	void EmpInfo(){
		
		System.out.println("Id is :" + empId);
		System.out.println("Name is :" + str);
	}
}

class MainDemo{
	
	public static void main(String[] args){
		
		Employee emp1 = new Employee();
		emp1.EmpInfo();
		System.out.println(emp1.empId);
		System.out.println(emp1.str);
	}
}
