/*Static variables and static method*/

/*here the scenario is that the static variables are initialized before everything their priority is very high and then static methods are initialized then*/

/*Static methods and variables can be directly accessed by the Class Name no objects are needed to call them*/
class StaticDemo{
	
	static int x = 10;
	static int y = 20;

	static void Disp(){
		
		System.out.println(x);
		System.out.println(y);
	}
}


class Client{
	
	public static void main(String[] args){
		System.out.println(StaticDemo.x);
		System.out.println(StaticDemo.y);

		StaticDemo.Disp();
	}	
}
