/*Using ststic and instance variables*/

class Employee{
	
	int empId = 10;
	String name = "Kanha";
	static int y = 50;

	void EmpInfo(){
	
		System.out.println("Employee Id :" + empId);
		System.out.println("employee name :" + name);
		System.out.println("y value is :" + y);
	}
}

class MainDemo{

	public static void main(String[] args){
	
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.EmpInfo();
		emp2.EmpInfo();

		emp2.empId = 20;
		emp2.name = "Rahul";
		emp2.y = 5000;

		emp1.EmpInfo();
		emp2.EmpInfo();

	}
}


