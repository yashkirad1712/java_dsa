/*Accessing Instance variable without making an object*/

class Demo{
	
	int x = 10;
	private int y = 20;

	void fun(){
		
		System.out.println(x);
		System.out.println(y);
	}
}

class MainDemo{

	public static void main(String[] args){
		
		Demo obj = new Demo();
		obj.fun();
		System.out.println(obj.x);
		System.out.println(obj.y);
		System.out.println(x);
		System.out.println(y);
		/* errors :program03.java:22: error: y has private access in Demo
                System.out.println(obj.y);
                                      ^
		program03.java:23: error: cannot find symbol
                System.out.println(x);
                                   ^
		  symbol:   variable x
		  location: class MainDemo
		program03.java:24: error: cannot find symbol
                System.out.println(y);
                                   ^
	  	symbol:   variable y
		location: class MainDemo
		3 errors*/
	}
}
