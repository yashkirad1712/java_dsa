/*Object Creation call the constructor implicitly cauz all the instance variable are need to be initalized and Constructor are used to initailze the them in the stack frame*/


class Demo{
	
	int x = 10;
	static int y = 20;

	static{
		
		System.out.println("Static block 1");
	}

	public static void main(String[] args){
		
		System.out.println("Main Method");
		Demo obj = new Demo();
		System.out.println(obj.x);
	}

	static {
		
		System.out.println("Static block  2");
		System.out.println(y);
	}
}
