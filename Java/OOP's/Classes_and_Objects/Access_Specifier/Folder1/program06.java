/*Static ,instance and private instance variables*/
/*If we change any instance variable of an object it doesnt showcase that change to other objects but if we change the static variable by any object then it showcase that change to every object*/
/*Example : instance is mobile and static is TV*/

class Demo{
	
	int x =10;
	private int y=20;
	static int z = 30;

	void Disp(){
		
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
}


class Client{
	
	public static void main(String[] args){
		
		Demo obj1 = new Demo();
		Demo obj2 = new Demo();

		obj1.Disp();
		obj1.x = 100;
		obj2.z = 300;

		obj1.Disp();
		obj2.Disp();
	}
}
