/*Initialization of the i=staic and instance variables and methods and the main as per priority */


/*
 *1. Static Varibale
  2. Static Block
  3. Static Method
  4. Main
  5. Instance Variable
  6. Instance Block
  7. Constructor
  8. Instance Method
 **/

class Demo{
	
	int x = 10;
	static int y = 20;
	
	static{ System.out.println("Static variable : " + y);}

	Demo(){
		
		System.out.println("In Constructor");
	}

	static {
		
		System.out.println("In Static block 1");
	}

	{
		
		System.out.println("In Instance Block 1");
	}

	public static void main(String[] args){
		
		Demo obj = new Demo();
		System.out.println("In main");
	}

	static {
		
		System.out.println("In Static Block 2");
	}

	{
		
		System.out.println("In Instance Block 2");
	}
}
