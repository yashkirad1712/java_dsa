/*Usage of static block in 2 class and main in 2 class*/


class Demo{
	
	static{
		
		System.out.println("In Static block 1");
	}

	public static void main(String[] args){
		
		System.out.println("In demo Main");
	}
}

class Client{
	
	static{
		
		System.out.println("In static block 2");
	}

	public static void main(String[] args){
		
		System.out.println("In Client Main");
	}

	static {
		
		System.out.println("In static block 3");
	}
}

class User{
	
	public static void main(String[] args){
		
		Client obj1 = new Client();
		obj1.main(new String[0]);

		Demo obj2 = new Demo();
		obj2.main(new String[0]);
	}
}
