/*static variable cannot be decalred in static block ,method or instance method or block it is a clss variable so only it can be declared in Class*/

class Demo{
	
	static int x = 20;

	static{
		
		static int y = 30;
	}

	void fun(){
		
		static int z = 40;
	}

	{
		static int a = 50;
	}

	static void gun(){
		
		static int b = 60;
	}
}
