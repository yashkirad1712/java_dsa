/*Class,object,Constructor,Instance Variable and Another Class*/

class Project{

	String projName = "Online Edu";
	int noOfEmployee = 20;

	void clientInfo(){
		
		String ClientName = "Core2Web";
		System.out.println(ClientName);
		System.out.println(projName);
		System.out.println(noOfEmployee);
	}
}

class C2W{
	
	public static void main(String[] args){
		
		Project obj = new Project();
		obj.clientInfo();
	}
}
