/*Showing that Constructor are always called intrnally when we create an object of the class*/

class IPL{
	
	IPL(){
		
		System.out.println("RCB");
	}

	public static void main(String[] args){
		
		IPL obj1 = new IPL();
		IPL obj2 = new IPL();
	}
}	
