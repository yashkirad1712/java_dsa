/*Static block comes first before the main methods block */


class Demo{
	
	static{
		int x = 10;
		System.out.println(x);
		System.out.println("First Static block of first class");
	}

	public static void main(String[] args){
		
		System.out.println("In main Method");
	}

	Demo1 obj = new Demo1();

	static{
		
		int y = 20;
		System.out.println(y);
		System.out.println("Second Static block of first class");
	}
	
}

class Demo1{
	
	static{
		int z = 30;
		System.out.println(z);
		System.out.println("First Static block of second Class");
	}
}
