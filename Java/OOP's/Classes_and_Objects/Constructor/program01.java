/*Creating the objects which will call the constructor and passing the paramters to it to change the instance private variables of the different class*/


class Player{
	
	private int jerNo = 0;
	private String name = null;

	Player(int jerNo ,String name){
		String str = "Mahi";
		this.jerNo = jerNo;
		this.name = name;
		System.out.println("In Constructor");
	}

	void info(){
		
		System.out.println(jerNo + " = " + name);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Player obj1 = new Player(18 , "Virat");  //Palyer(obj1,18,"Virat")
		obj1.info();				//info(obj1)	
		
		
		Player obj2 = new Player(7 , "MSD");  //Palyer(obj2,7,"MSD")
		obj2.info();			      //info(obj2)	
		
		Player obj3 = new Player(45 , "Rohit");  //Palyer(obj3,45,"Rohit")	
		obj3.info();				//info(obj3)	
	
	}
}
