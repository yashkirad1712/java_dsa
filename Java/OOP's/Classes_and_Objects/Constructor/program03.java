/*Showing that we can use Constructor to change the instance private value from outside of class with the help of the constructor */


class Demo{
	
	private int jerNo = 0;
	private String Name = null;

	Demo(int jerNo , String Name){
		
		this.jerNo =  jerNo;
		this.Name = Name;
	}

	void Disp(){
		
		System.out.println("Jersey Number is : " + jerNo);
		System.out.println("Name of Player is : " + Name);
	}
}

class Client{
	
	public static void main(String[] args){
		

		Demo plyr1 = new Demo(18,"Virat");
		plyr1.Disp();
		
		Demo plyr2 = new Demo(7,"MSD");
		plyr2.Disp();
		
		Demo plyr3 = new Demo(45,"Rohit");
		plyr3.Disp();
	}
}
