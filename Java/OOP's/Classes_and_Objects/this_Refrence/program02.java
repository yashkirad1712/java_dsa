/*Method Signature : Every method has a unique method signature of itself in the method signature table if ther are dublicte emthod signatures present then it will give an error at compile time only*/

 class Demo{
 	
	 int a = 10;

	 Demo(){
	 	System.out.println("In Constructor 1");
	 	System.out.println(a);
	 }

	 Demo(){
	 
	 	System.out.println("In Constructor 2");
	 	System.out.println(a);
	 }
 }

/* error:program02.java:12: error: constructor Demo() is already defined in class Demo
         Demo(){
         ^
1 error}
*/
