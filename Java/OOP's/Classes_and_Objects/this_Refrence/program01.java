/*Usage of this :
 * 1. Initialization of teh instance variable in the Constructor 
 * 2. Change the instance private data of the class with the help of the constructor*/

class Player{
	
	private int jerNo = 0;
	private String Name = null;

	Player(int jerNo , String Name){
		
		this.jerNo =  jerNo;
		this.Name = Name;

		System.out.println("In Constructor");
	}

	void Info(){
		
		System.out.println(jerNo + " = " + Name);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Player obj1 = new Player(18,"Virat");   //Player(obj1,18,"Virat")
		obj1.Info();				//info(obj1)
		
		Player obj2 = new Player(7,"MSD");      //Player(obj2,7,"MSD")
		obj2.Info();				//info(obj2)
		
		Player obj3 = new Player(45,"Rohit");   //Player(obj3,45,"Rohit")
		obj3.Info();				//info(obj3)
	}
}
