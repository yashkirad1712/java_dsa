/*In a Constructor either firt line Should be the InvokeSpecial or call to another constructor by this but not both*/

/*Concept of Recursive calls in This */


class Demo{
	
	int x = 10;

	Demo(){
		this(x);
		super();
		/*program05.java:11: error: cannot reference x before supertype constructor has been called
                this(x);
                     ^
		program05.java:12: error: call to super must be first statement in constructor
                super();
                     ^
		program05.java:14: error: call to this must be first statement in constructor
                this(20);
                    ^
		3 errors*/	
		//this.x = 20;
		this(20);
		System.out.println(x);

	}

	Demo(int x){
		//this();
		System.out.println("In para Constructor");
	}

	public static void main(String[] args){
		
		Demo obj = new Demo();
	}
}

/*Error is : program05.java:10: error: recursive constructor invocation
                this(20);
                ^
1 error*/
