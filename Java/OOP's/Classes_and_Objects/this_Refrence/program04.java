/*Showing how this refrence can be used to call Constructor from another Constructor known as Constructor Linking*/

class Demo{

	int x = 10;

	Demo(){
		System.out.println("in No Args Constructor");
	}

	Demo(int x){
		this();
		System.out.println("In Para Constructor");
	}

	public static void main(String[] args){
		
		Demo obj = new Demo(50);
	}
}
