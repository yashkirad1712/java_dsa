/*An Abstract class is  a class in which a method is just defines not initialized*/

/*We make a class Abstract because when we know child is definitely going to override the parents method then we use abstraction*/

abstract class Parent{
	
	void Carrier(){
		System.out.println("Doctor");
	}

	abstract void Marry();
}

class Child extends Parent{
	
	void Marry(){
		
		System.out.println("Alexandra Dadario");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();
		obj.Carrier();
		obj.Marry();

		Parent obj1 = new Child();
		obj.Marry();
	}
}
