/*Covarient Return Types when different return types are there in overriding is valid*/

class Parent{
	
	Object fun(){
		
		System.out.println("In Parent");
		return new Object();
	}
}

class Child extends Parent{
	 
	String fun(){
		
		System.out.println("In Child");
		return "Yash";
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj = new Child();
		obj.fun();
	}
}

/*The only condition in covarient return types is that parent must have the superior return type then the child*/
