/*Checking the Refrence and Object i.e created is according to the compilers script or it is not matching it */

/*Proof that compilers check the code according to the Refrences not objects*/

class Parent{

	Parent(){
		
		System.out.println("In Parent Constructor");
	}

	void fun(int x){
		
		System.out.println("In Parents Fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In Child Constructor");
	}

	void fun(){
		
		System.out.println("In Child Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Child();
		obj1.fun();
	}
}

/*in this code at compile time the compiler will check the fun method as per the parents method which is fun (int) and i.e why this code will not run as the parents fun method is of fun(int) but we are calling it by fun(int) and  child side fun method is not accessible to the parent as it came after parent and this is not checked by compiler but checked by the JVM but at compile time instead of JVM compiler do this so there is a error */
