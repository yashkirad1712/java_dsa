/*Access Modifiers dont play any role in Ovverriding as they restrict the method from being overriden*/

class Parent{
	
	final void fun(){
		
		System.out.println("Parent Fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child Fun");
	}
}

/*program16.java:13: error: fun() in Child cannot override fun() in Parent
        void fun(){
             ^
  overridden method is final
1 error*/
