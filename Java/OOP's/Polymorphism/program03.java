/*Every time when a class is created a method table is created of it which hold the value of all the methods in it at compile time only it craeted and checked that correct call is passed or not so if we try to call the child method from the parent method it will give the error*/


class Parent{
	
	Parent(){
		
		System.out.println("In parent Constructor");
	}

	void fun(){
	
		System.out.println("In Fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In Child Constructor");
	}

	void gun(){
	
		System.out.println("In Gun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();/*Error Cannot find Symbol*/
	}
}
