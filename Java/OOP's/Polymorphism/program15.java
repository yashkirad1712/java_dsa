/*Access Specifiers do play role in the Overriding as we cannot reduce the access specifier of parent class in child class but we can increase the scope of parent class in child class*/

class Parent{
	
	private void fun(){
		
		System.out.println("In Parent");
	}
}

class child extends Parent{
	
	void fun(){
		
		System.out.println("In Child");
	}
}

class Client{
	
	public static void main(String[] args){
		Parent obj = new Parent();
		obj.fun();
	}
}

/*When a method is private then it doesnt come in the picture also as private methods are not given to child class so overriding them is not possible so it is an error*/

/*program15.java:23: error: fun() has private access in Parent
                obj.fun();
                   ^
1 error*/
