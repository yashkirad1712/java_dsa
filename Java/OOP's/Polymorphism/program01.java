/*Polymorphism: method Overloading : using same method for different inputs*/

class Demo{
	
	void fun(int x){
		System.out.println(x);
	}
	
	void fun(float x){
		System.out.println(x);
	}
	
	void fun(Demo x){
		System.out.println("In demo fun");
		System.out.println(x);
	}
	
	public static void main(String[] args){
		
		Demo obj = new Demo();
		obj.fun(10);
		obj.fun(50.69f);

		obj.fun(obj);
	}
}
