/*Ambiuous Scenario in String*/

class Demo{

	void fun(String str){
		System.out.println("String");
	}
	
	void fun(StringBuffer str){
		System.out.println("StringBuffer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new Demo();
		obj.fun("yash");
		obj.fun(new StringBuffer("Yk only"));
		obj.fun(null);

		/* String str= null
		 * StringBuffer str = new StringBuffer(null)*/
	}
}
/*program11.java:21: error: reference to fun is ambiguous
                obj.fun(null);
                   ^
  both method fun(String) in Demo and method fun(StringBuffer) in Demo match
1 error*/
