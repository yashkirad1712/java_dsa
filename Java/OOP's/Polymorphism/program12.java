/*object class can take parameters of nay child class of it*/

class Demo{
	
	void fun(Object obj){
		System.out.println("Object Class");
	}
	
	void fun(String str){
		System.out.println("Child Class");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new Demo();

		obj.fun("Yash");
		obj.fun(new StringBuffer("Yash"));
		obj.fun(null);
	}

}

/*When the parameter is matched by both parent and child class priority is given to the child class*/
