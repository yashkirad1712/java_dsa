/*Cricket Match Overriding EXample*/


class Match{
	void Matchtype(){
		System.out.println("T20/ODI/Test");
	}	
}

class IPLMatch extends Match{
	
	void Matchtype(){
		System.out.println("T20");
	}
}

class TestMatch extends Match{
	
	void Matchtype(){
		System.out.println("Test");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Match type1 = new Match();
		Match type2 = new IPLMatch();
		Match type3 = new TestMatch();

		type1.Matchtype();
		type2.Matchtype();
		type3.Matchtype();
	}
}
