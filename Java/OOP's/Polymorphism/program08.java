/*Real time Example of Overloading*/

class IPL{
	
	void MatchInfo(String team1 ,String team2){
		
		System.out.println(team1 + " vs " + team2);
	}

	void MatchInfo(String team1 ,String team2 ,String Venue){
		
		System.out.println(team1 + " vs " + team2);
		System.out.println("Venue: " + Venue);
	}
}

class Client{
	
	public static void main(String[] args){
		
		IPL ipl2023 = new IPL();
		ipl2023.MatchInfo("GT","RCB");
		ipl2023.MatchInfo("GT","RCB","NMSA");
	}
}

/* MethodSignature Matters Return type dosent matter in Overloading*/
