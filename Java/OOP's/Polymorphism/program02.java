/*Method Overloading Example Method Marry in the following Eaxmple*/

class Parent{
	
	Parent(){
	
		System.out.println("Parent Constructor");
	}

	void Property(){
		
		System.out.println("Car,Gold,Home");
	}

	void Marry(){
		
		System.out.println("Deepika Padukone");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In Child Constructor");
	}

	void Marry(){  //method Overriding will be done childs marry method will be executed if MArry is called.
	
		System.out.println("Anushka Sen");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();
		obj.Property();
		obj.Marry();
	}
}
