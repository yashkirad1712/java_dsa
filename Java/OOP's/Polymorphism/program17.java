/*Access Modifiers dont play any role in Ovverriding as they restrict the method from being overriden*/

class Parent{
	
	static void fun(){
		
		System.out.println("Parent Fun");
	}
}

class Child extends Parent{
	
	static void fun(){
		
		System.out.println("Child Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Parent();
		obj1.fun();
		
		Child obj2 = new Child();
		obj2.fun();

		Parent obj3 = new Child();
		obj3.fun();
	}
}

/*Static methods are Class methods and they are linked with the object at compile time only*/
