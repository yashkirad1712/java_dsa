/*Checking the Refrence and Object i.e created is according to the compilers script or it is not matching it */

/*Proof that compilers check the code according to the Refrences not objects*/

class Parent{

	Parent(){
		
		System.out.println("In Parent Constructor");
	}

	void fun(){
		
		System.out.println("In Parents Fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In Child Constructor");
	}

	void fun(){
		
		System.out.println("In Child Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Child();
		obj1.fun();
	}
}

/*in this code at compile time the compiler will check the fun method as per the parents method which is fun (void) and i.e why this code will run but at run time the actual child side fun method will also come in the picture but as the parents method are accessible to child i.e why child can acces the Parenrs fun method but here as both have same method signature as fun(void) parents method is override by the childs method and chil method is given to that call*/




