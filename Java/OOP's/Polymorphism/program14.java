/*Access Specifiers do play role in the Overriding as we cannot reduce the access specifier of parent class in child class but we can increase the scope of parent class in child class*/

class Parent{
	
	public void fun(){
		
		System.out.println("In Parent");
	}
}

class child extends Parent{
	
	void fun(){
		
		System.out.println("In Child");
	}
}

/*program14.java:13: error: fun() in child cannot override fun() in Parent
        void fun(){
             ^
  attempting to assign weaker access privileges; was public
1 error*/
