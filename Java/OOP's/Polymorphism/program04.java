/*An refrencde of PArent and the object of the child class is ok but refrence of the child class and object of the Parent is not valid*/


/*Child obj =  new Parent()  : Incompatible Types*/

/*At Compilation time things are checked as per the left side of the equal to sign i.e refrences not objects cauz obj is created at runtime*/


class Parent{
	
	Parent(){
	
		System.out.println("In Parent Constructor");
	}

	void fun(){
		
		System.out.println("In Parent Fun");
	}
}

class Child extends Parent{

	Child(){
		System.out.println("In Child Constructor");
	}

	void fun(){
		
		System.out.println("In Parent Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj = new Child();
		obj.fun();
	}
}
