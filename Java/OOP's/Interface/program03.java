/*Static Interface: child class is not given the methods of parent class*/

class Demo{
	
	static void Fun(){
		
		System.out.println("IN fun");
	}
}

class DemoChild extends Demo{
	
	
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj = new DemoChild();
		obj.Fun();
	}

}

/*In Class static method behaves differently it is passed to its child by the parent*/
