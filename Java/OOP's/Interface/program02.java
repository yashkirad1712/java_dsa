/*Static Interface: child class is not given the methods of parent class*/

interface Demo{
	
	static void Fun(){
		
		System.out.println("IN fun");
	}
}



class DemoChild implements Demo{
	
	
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj = new DemoChild();
		obj.Fun();
	}

}

/*program02.java:21: error: cannot find symbol
                obj.Fun();
                   ^
  symbol:   method Fun()
  location: variable obj of type DemoChild
1 error*/
