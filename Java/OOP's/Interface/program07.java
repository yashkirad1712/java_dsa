/*Default Keyword in java*/

interface Demo1{
	
	default void fun(){
		System.out.println("In Demo1 - fun");
	}
}

interface Demo2{
	
	default void fun(){
		System.out.println("In Dmeo2 - fun");
	}
}

class DemoChild implements Demo1,Demo2{

	public void fun(){
		
		System.out.println("In DemoChild -Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo1 obj1 = new DemoChild();
		obj1.fun();
		Demo2 obj2 = new DemoChild();
		obj2.fun();
		DemoChild obj = new DemoChild();
		obj.fun();
	}
}
