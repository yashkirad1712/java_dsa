/*Static Interface: child class is not given the methods of parent class*/

interface Demo1{
	
	default void fun(){
		
		System.out.println("In Demo1 - fun");
	}
}

interface Demo2{
	
	default void fun(){
		
		System.out.println("In Demo2 - fun");
	}

}

interface DemoChild extends Demo1,Demo2{
	
	
}

class Client{
	
	public static void main(String[] args){
		
		Demo1 obj = new Client();
		obj.fun();
	}

}


/*program04.java:29: error: incompatible types: Client cannot be converted to Demo1
                Demo1 obj = new Client();
                            ^
program04.java:30: error: illegal static interface method call
                obj.fun();
                       ^
  the receiver expression should be replaced with the type qualifier 'Demo1'
2 errors*/
