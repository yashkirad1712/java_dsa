class Parent {
    private int value;

    public Parent(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

class Child extends Parent {
    public Child() {
        super(0); // Passing default value to parent constructor
    }

    public boolean Check(Parent obj1) {
        int parentValue = obj1.getValue();
        int superValue = super.getValue();
        
        return parentValue == superValue;
    }
}

class Client{
    public static void main(String[] args) {
        Parent obj1 = new Parent(0); // Parent object with default value
        Child obj2 = new Child();
        
        boolean Result = obj2.Check(obj1);
        System.out.println("Is default value equal: " + Result);
    }
}

