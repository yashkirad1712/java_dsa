/*Static in Parent class: Static blocks of different class dont get merged but the static block of the same class gets merged*/

class Parent{

	static{
		System.out.println("In parent static block");
	}
}

class Child extends Parent{

	static{
		System.out.println("In child static Block");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();
	}
}
