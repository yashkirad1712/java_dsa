/*Static and instance*/

class Parent{
	
	int x = 10;
	static int y = 20;

	static{
		System.out.println("In parent static block");
	}

	Parent(){
		
		System.out.println("In Parent Constructor");
	}
	
	void MethodOne(){
		
		System.out.println(x);
	}

	static void MethodTwo(){
		
		System.out.println(y);
	}
}

class Child extends Parent{

	static{
		System.out.println("In child static Block");
	
	}

	Child(){
		
		System.out.println("In Child Constructor");
	}
}

class Client{
	
	public static void main(String[] args){
		System.out.println("In Main");
		Child obj = new Child();
		obj.MethodOne();
		obj.MethodTwo();
	}
}
