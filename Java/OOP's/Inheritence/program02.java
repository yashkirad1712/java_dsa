/*Inheritence:  Creating only one object of child class and initializing the instance things of parent calss as well by creating a single object*/

class Parent{
	
	Parent(){  //parent(parent this ****where this is 100)
		
		System.out.println("In Parent Constructor");
	}

	void ParentProperty(){
		System.out.println("Flat,Car,Gold");
	}
}

class Child extends Parent{
	
	
	Child(){   //child(obj i.e 100)
		   
		//super(obj i.e 100 only)
		
		System.out.println("In Child Constructor");
		
	}

}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child(); //child(obj i.e 100)
		obj.ParentProperty();    // ParentProperty(obj i.e 100 to parent constructor address and from there to that respective methods byte code)
	}
}
