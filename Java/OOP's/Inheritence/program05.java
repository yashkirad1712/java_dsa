/*Static in Parent class: there is a seperate section in childs spcl structure which points towards the parent spcl structure so whenever the child object is created it checks whether there is any static block present in parents class if yes it execute it first and then execute the childs static block*/

class Parent{
	
	static int x = 10;

	static{
		System.out.println("In parent static block");
	}

	static void access(){
		
		System.out.println(x);
	}
}

class Child extends Parent{

	static{
		System.out.println("In child static Block");
		System.out.println(x);
		access();
	}
}

class Client{
	
	public static void main(String[] args){
		System.out.println("In Main");
		Child obj = new Child();
	}
}
