/*When we make a class parent of another class then creatring the object of child can help us to access the object of the parent class*/
/*When we call the parent class from the child class then the parameter is this of child but the container is Parent class which is ok but vice versa is not allowed in java
 *
 * class parent(parent this){}  -->childs this
 * 
 * class Child extends parent{}
 *
 * but 
 * class parent{}  
 * 
 * class Child extends parent(child this) --> parents this{}
 *
 * is not ok*/

class Parent{
	
	int x = 10;
	Parent(){
	
		System.out.println("In Parent Constructor");
	}

	void access(){
		
		System.out.println("In Parent Instance");
	}
}

class Child extends Parent{

	int y = 20;

	Child(){
		
		System.out.println("In child Constructor");
		System.out.println(x);
		System.out.println(y);
	}

}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();
		obj.access();
	}
}









