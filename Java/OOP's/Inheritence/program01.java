/*Real Time Example of Inheritence*/


class Govt_Of_India{
	
	String Citizenship = "Indian";

	Govt_Of_India(){
		System.out.println("Representing India");
	}

	void Global_Identity(){
		System.out.println(" |-> Citizenship of the Employee is : " + Citizenship);
	}
	
	
}

class FoodSector extends Govt_Of_India{

	FoodSector(){
		System.out.println(" --In Food Sector-- ");
	}

	void My_Market(){
		
		System.out.println("1 . Vegetables prices hikes by 10%");
		System.out.println("2 . Imported Food Items reduces Market price By 20%");
	}
	
}

class TourismSector extends Govt_Of_India{
	
	TourismSector(){
		
		System.out.println(" --In Tourism Sector-- ");
	}

	void Place_Study(){
		
		System.out.println("1 . Govt.  Stops toursim of Xyz place");
		System.out.println("2 . Maharashtra is Open for Tourists");
	}

	
}

class CrudeOilSector extends Govt_Of_India{
	
	CrudeOilSector(){
		
		System.out.println(" --In Crude Oil Sector-- ");
	}

	void Product_Study(){
		
		System.out.println("1 . Petrol Price hikes by 11.5%");
		System.out.println("2 . Diesel Price reduces by 2.5%");
	}
	

}

class Client{
	
	public static void main(String[] args){
		
		FoodSector emp1 = new FoodSector();
		emp1.Global_Identity();
		emp1.My_Market();
		System.out.println(" -*-*-*-*-*-*-*- ");	
		System.out.println();	
		
		TourismSector emp2 = new TourismSector();
		emp2.Global_Identity();
		emp2.Place_Study();
		System.out.println(" -*-*-*-*-*-*-*- ");	
		System.out.println();	
	
		CrudeOilSector emp3 = new CrudeOilSector();
		emp3.Global_Identity();
		emp3.Product_Study();
		System.out.println(" -*-*-*-*-*-*-*- ");	
		System.out.println();	
	}
}

