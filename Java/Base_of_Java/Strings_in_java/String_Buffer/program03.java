/*We can also declare the size of the StringBuffer as per our convineince also this is the advantage of the StringBuffer as it does not cause any repeated calculation of size*/

class Yash{
	
	public static void main(String args[]){
		
		StringBuffer sb = new StringBuffer(100);
		sb.append("Biencaps");
		sb.append("Core2web");

		System.out.println(sb);
		System.out.println(sb.capacity());
		sb.append("Incubators");

		System.out.println(sb);
		System.out.println(sb.capacity());
	}
}
