/*Formula for finding the Capacity of the StringBuffer object :
  (currentcapacity + 1) * 2
  
  */

class Yash{
	
	public static void main(String args[]){
		
		StringBuffer sb = new StringBuffer();

		System.out.println("The Default Capacity of the String Buffer Object is :" + sb.capacity());

		sb.append("abcdef");
		System.out.println("The capacity after 1st append becomes :" + sb.capacity());
		System.out.println(sb);
		sb.append("12345");
		System.out.println("The capacity after 2nd append becomes :" + sb.capacity());
		System.out.println(sb);
		sb.append("6789101112");
		System.out.println("The capacity after 3rd append becomes :" + sb.capacity());
		System.out.println(sb);
		
		

	}
}
