/*String Buffer Object
 *
 * Always Create Object of String Buffer cauz it is different from String class*/

class Yash{
	
	public static void main(String args[]){
		

		StringBuffer str1 = new StringBuffer("Yash");

		System.out.println(System.identityHashCode(str1));
		str1.append("Kirad");

		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
	}
}

