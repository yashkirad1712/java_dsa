/*My methods*/


import java.io.*;

class Yash{

	static String myAppend(String str1, String str2) {
    		int len1 = str1.length();
		int len2 = str2.length();
    		char[] result = new char[len1 + len2];

    
	 	for (int i = 0; i < len1; i++) {
        		result[i] = str1.charAt(i);
    		}

    
    		for (int i = 0; i < len2; i++) {
        		result[len1 + i] = str2.charAt(i);
    		}

    		return new String(result);

	}

	static String MyReverse(String str) {
    		int len = str.length();
    		char[] result = new char[len];

       		for (int i = 0; i < len; i++) {
        		result[i] = str.charAt(len - i - 1);
    		}

    		return new String(result);
	}
	static String MyDelete(String str, int start, int end) {
		int len = str.length();
    		char[] result = new char[len - (end - start)];

    
    		for (int i = 0; i < start; i++) {
        		result[i] = str.charAt(i);
    		}

    
    		for (int i = end; i < len; i++) {
        		result[i - (end - start)] = str.charAt(i);
    		}

    		return new String(result);
	}

 	static String MyReplace(String str, int start, int end, String replacement) {
    		int len = str.length();
    		char[] result = new char[len - (end - start) + replacement.length()];

    
    		for (int i = 0; i < start; i++) {
        		result[i] = str.charAt(i);
    	}

    
    		int j = start;
    		for (int i = 0; i < replacement.length(); i++) {
        		result[j++] = replacement.charAt(i);
    		}

    
    		for (int i = end; i < len; i++) {
       			 result[j++] = str.charAt(i);
    		}

    		return new String(result);
	}

	static String MyInsert(String str, int index, String insertStr) {
    		int len = str.length();
    		int insertLen = insertStr.length();
    		char[] result = new char[len + insertLen];

        		for (int i = 0; i < index; i++) {
     	   		result[i] = str.charAt(i);
    		}

    
    		for (int i = 0; i < insertLen; i++) {
        		result[index + i] = insertStr.charAt(i);
    		}

    
    		for (int i = index; i < len; i++) {
        		result[insertLen + i] = str.charAt(i);
    		}

    		return new String(result);
	}
	
	static void myStrCompare(String str1,String str2){
		
		int flag = 1;

		char[] ch1 = str1.toCharArray();
		char[] ch2 = str2.toCharArray();

		if(ch1.length == ch2.length){
			for(int i=0 ;i<ch1.length ;i++){
				if(ch1[i] != ch2[i]){
					int ret = ch1[i] - ch2[i];
					System.out.println(ret);
					flag = 0;
					break;
				}
			}
			if(flag == 1) System.out.println(0);
		}else{
			if(ch1.length > ch2.length){
				int j = 0;
				for(;j<ch2.length ;j++){
					if(ch1[j] != ch2[j]){
						int ret = ch1[j] - ch2[j];
						System.out.println(ret);
						flag = 0;
						break;
					}
				}
				if(flag == 1){
					int num = 0 - ch1[j];
					System.out.println(num);
				}
			}else{
				
				int j = 0;
				for(;j<ch1.length ;j++){
					if(ch2[j] != ch1[j]){
						int ret = ch2[j] - ch1[j];
						System.out.println(ret);
						flag = 0;
						break;
					}
				}
				if(flag == 1){
					int num = 0 - ch2[j];
					System.out.println(num);
				}
			
			}

		}
	}

	static char myCharAt(String str,int index){
		
		char[] ch = str.toCharArray();
		return ch[index];
	}

	static int myCompareToIgnoreCase(String str1 ,String str2){
		
		char[] ch1 = str1.toCharArray();
		char[] ch2 = str2.toCharArray();

		int len = Math.min(ch1.length,ch2.length);
			
		for(int i=0 ;i<len ;i++){
			char val1 = Character.toUpperCase(ch1[i]);
			char val2 = Character.toUpperCase(ch2[i]);

			if(val1 != val2){
				return val1 - val2;
			}
		}
		
		if(ch1.length == ch2.length){
			return 0;
		}

		return ch1.length - ch2.length;	
	}

	static boolean myEquals(String str1 ,String str2){
		
		int len1 = myStrLen(str1);
		int len2 = myStrLen(str2);

		if(len1 != len2){
			return false;
		}else{
			char[] ch1 = str1.toCharArray();
			char[] ch2 = str2.toCharArray();

			for(int i=0 ;i<len1;i++){
				if(ch1[i] != ch2[i]){
					return false;
				}
			}
		}
		return true;
	}
	
	static boolean myEqualsIgnoreCase(String str1 ,String str2){
		
		int len1 = myStrLen(str1);
		int len2 = myStrLen(str2);

		if(len1 != len2){
			return false;
		}else{
			char[] ch1 = str1.toCharArray();
			char[] ch2 = str2.toCharArray();

			for(int i=0 ;i<len1;i++){
				char val1 = Character.toUpperCase(ch1[i]);
				char val2 = Character.toUpperCase(ch2[i]);
				if(val1 != val2){
					return false;
				}
			}
		}
		return true;
	}

	static int myIndexOf(String str,int index,char val){
		
		char[] ch = str.toCharArray();

		for(int i=index ;i<ch.length;i++){
			if(ch[i] == val){
				return i;
			}
		}

		return -1;

	}
	
	static int myLastIndexOf(String str,int index,char val){
		
		char[] ch = str.toCharArray();

		for(int i=0 ;i<index;i++){
			if(ch[i] == val){
				return i;
			}
		}

		return -1;

	}
	
	static String myStringReplace(String str,char val1,char val2){
		
		char[] ch = str.toCharArray();

		for(int i=0 ;i<ch.length;i++){
			if(ch[i] == val1){
				ch[i] = val2;
			}
		}
		
		String str1 = new String(ch);
		return str1;

	}

	static String myStringSubString(String str,int start,int end){
		
		char[] ch1 = str.toCharArray();
		char[] ch2 = new char[ch1.length];
		
		int itr = 0;
		for(int i=start; i<end ;i++){
			ch2[itr] = ch1[i];
			itr++;
		}

		String str1 = new String(ch2);
		return str1;
	}



	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Yash obj = new Yash();
		char ch;
		do{
		
			System.out.println("|~~~~~~~~~~~~~~~~~~~~~~~~~~|");
			System.out.println("|1.  myStrlen              |");
			System.out.println("|2.  myStrConcat           |");
			System.out.println("|3.  myStrCompare          |");
			System.out.println("|4.  myCharAt              |");
			System.out.println("|5.  myCompareToIgnoreCase |");
			System.out.println("|6.  myEquals              |");
			System.out.println("|7.  myEqualsIgnorecase    |");
			System.out.println("|8.  myIndexOf             |");
			System.out.println("|9.  myLastIndexOf         |");
			System.out.println("|10. myStringReplace       |");
			System.out.println("|11. myStringSubString     |");
			System.out.println("|~~~~~~~~~~~~~~~~~~~~~~~~~~|");
			
			System.out.println("Choose the Number to continue");
			int choice = Integer.parseInt(br.readLine());
			switch(choice){
				
				case 1:
			
					{
					System.out.println("Enter the String");
					String str = br.readLine();
					int count = myStrLen(str);

					System.out.println("The lenght of the string is : " + count);
					}
					break;
				
				case 2 :
					{
					System.out.println("Enter the String One");
					String str1 = br.readLine();
	
					System.out.println("Enter the String Two");
					String str2 = br.readLine();
					myStrConcat(str1,str2);
					}
					break;
				case 3:
					{
					System.out.println("Enter the String One");
					String str1 = br.readLine();
	
					System.out.println("Enter the String Two");
					String str2 = br.readLine();
					myStrCompare(str1,str2);
					}
					break;
				case 4 :
					{
						System.out.println("Enter the String");
						String str = br.readLine();

						System.out.println("Enter the index");
						int index = Integer.parseInt(br.readLine());
						if(index >= str.length() || index < 0){
							System.out.println("Invalid Index");
						}else{
							char ret = myCharAt(str,index);
							System.out.println("The Character at index " + index + " is : " + ret);
						}	
					}
					break;
				case 5 :
					{
						System.out.println("Enter the 1st string");
						String str1 = br.readLine();

						System.out.println("Enter the 2nd string");
						String str2 = br.readLine();

						int ret = myCompareToIgnoreCase(str1,str2);
						System.out.println("The Difference is : " + ret);
					}
					break;	
				case 6 :
					{
						System.out.println("Enter the 1st String");
						String str1 = br.readLine();
						
						System.out.println("Enter the 2nd String");
						String str2 = br.readLine();

						boolean ret = myEquals(str1 ,str2);
						System.out.println(ret);
					}
					break;
				case 7 :
					{
						System.out.println("Enter the 1st String");
						String str1 = br.readLine();
						
						System.out.println("Enter the 2nd String");
						String str2 = br.readLine();

						boolean ret = myEqualsIgnoreCase(str1 ,str2);
						System.out.println(ret);
					}
					break;
				case 8 :
					{
						System.out.println("Enter the String");
						String str = br.readLine();
						
						System.out.println("Enter the index to Search with");
						int index = Integer.parseInt(br.readLine());
						
						if(index >= str.length() || index < 0){
							System.out.println("Invalid Index");
						}else{
							System.out.println("Enter the Character to Search");
							char val = (char)br.read();
							br.skip(1);
	
							int ret = myIndexOf(str,index,val);
							System.out.println(ret);
						}
					}	
					break;
				case 9 :
					{
						System.out.println("Enter the String");
						String str = br.readLine();
						
						System.out.println("Enter the index upto Search");
						int index = Integer.parseInt(br.readLine());
						
						if(index >= str.length() || index < 0){
							System.out.println("Invalid Index");
						}else{
							System.out.println("Enter the Character to Search");
							char val = (char)br.read();
							br.skip(1);
	
							int ret = myLastIndexOf(str,index,val);
							System.out.println(ret);
						}
					}	
					break;	
				case 10 :
					{
						System.out.println("Enter the String");
						String str = br.readLine();

						System.out.println("Enter the character to act on");
						char val1 = (char)br.read();
						br.skip(1);
						
						System.out.println("Enter the character to replace");
						char val2 = (char)br.read();
						br.skip(1);

						String ret = myStringReplace(str,val1,val2);
						System.out.println(ret);

					}
					break;
				case 11 :
					{
						System.out.println("Enter the String");
						String str1 = br.readLine();

						System.out.println("Enter the Staring Index");
						int start = Integer.parseInt(br.readLine());
					
						int maxIndex = str1.length();	
						System.out.println("The total index present is :" + (maxIndex - 1));
						System.out.println("Enter the Ending Index");
						int end = Integer.parseInt(br.readLine());
						
						if((start < 0 || end > maxIndex) || (start >= maxIndex || end < 0)){
							System.out.println("Invalid Conditions");
						}else{	
							String ret = myStringSubString(str1,start,end);
							System.out.println(ret);
						}	
					}
					break;	

				default : 
					System.out.println("Invalid Input");

			}

			System.out.println("Do you want to continue...");
			ch =(char)br.read();
			br.skip(1);
		}while(ch == 'y' || ch == 'Y');
	}
}
