/*String Introduction*/


class Yash{
	public static void main(String args[]){
		
		String str1 = "Core2Web"; 			//String constant pool
		String str2 = new String("Core2Web");		//Heap
                String str3 = "Core2Web";
		String str4 = new String("Core2Web");		//Heap

		//char str3[] = {'C','2','W'}; //op:C2W

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		
	}
}
