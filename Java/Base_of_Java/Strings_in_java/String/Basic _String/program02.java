/*method .toString()*/

class Yash{
	
	public static void main(String args[]){
		
		char[] ch = {'y','a','s','h'};

		System.out.println("Array :" + ch);// here internally ch.toString() method is called which displays the Address like value of the array not the output;
		
		char[] ch1 = {'y','a','s','h'};
	
		
		System.out.println(ch1.toString());
		System.out.println(System.identityHashCode(ch));
		System.out.println(System.identityHashCode(ch1));
		
	}
}
