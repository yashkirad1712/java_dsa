/* Strings are Immutable*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Yash";
		String str2 = "Kirad";

		String str3 = "Yash Kirad";
		String str4 = " ";
		String str5 = str1 + str4 +str2;

		System.out.println(str3 +" = " + System.identityHashCode(str3));
		System.out.println(str5 +" = " + System.identityHashCode(str5));
	}
}
