/*String as a value*/

class Yash{
	
	public static void main(String args[]){
	
		String str1 = "Kanha";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println(System.identityHashCode(str1) + "= " + str1);
		System.out.println(System.identityHashCode(str2) + "= " + str2);
		System.out.println(System.identityHashCode(str3) + "= " + str3);
	}
}
