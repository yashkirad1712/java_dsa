/*value of method*/

class Yash{
	
	public static void main(String args[]){
		
		int x = 1000; //primitive datatype
		Integer y = 1000; // class 
		
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));

		/*
		op:
		498931366
		2060468723
		622488023
		2060468723*/

		int[] arr = {1,2,3,4,5};
		Integer[] arr1 = {9,8,7,6};
		
		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr1));
		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr1));

	}
}
