/* Concat method*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Yash";
		String str2 = "Kirad";

		String str3 = "YashKirad";
	
		String str4 = str1.concat(str2);

		System.out.println(str3 +" = " + System.identityHashCode(str3));
		System.out.println(str4 +" = " + System.identityHashCode(str4));//new String object is created
	}
}
