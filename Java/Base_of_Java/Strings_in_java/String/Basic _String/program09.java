/*HashCode: it deals with the content of the given data i.e if the string are equal in the hashcode then it will shows same*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Shashi";
		String str2 = new String("Shashi");
		String str3 = "Shashi";
		String str4 = new String("Shashi");

		System.out.println(str1.hashCode());       //-1819698008
		System.out.println(str2.hashCode());       //-1819698008
		System.out.println(str3.hashCode());       //-1819698008
		System.out.println(str4.hashCode());       //-1819698008
		
		/*
		 
		int x = 10;
		int y = 10;
		error: int cannot be dereferenced
                System.out.println(x.hashCode());
                                    ^
                error: int cannot be dereferenced
                System.out.println(y.hashCode());
                                    ^                */

		Integer x = 10;
		Integer y = 10;
		Integer u = 1000;
		Integer v = 1000;

	       
		System.out.println(x.hashCode());
		System.out.println(y.hashCode());
		System.out.println(u.hashCode());
		System.out.println(v.hashCode());
	} 
}
