/* Concat method*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Yash";
		String str2 = "Kirad";

		String str3 = "YashKirad";
	
		System.out.println(str1);
		System.out.println(str2);
	 	
		str1.concat(str2);

		System.out.println(str1);
		System.out.println(str2);
	}
}
