/*Method :public int compareTo(String str2)
 *
 - it compares the str1 & str2(case Sensitive) if both are equal it returns 0 otherwise return the difference of their uncommon alphabets or character

parameter : String(Second string)
return type : integer*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Yash";
		String str2 = "Aryan";

		System.out.println(str1.compareTo(str2));
	}
}
