/*Method : public boolean equals(Object anObject)
 *
 - predicate which compares an objects to this,this is only for strings with the same character sequence returns true if an object is semantically equal to this 

Parameter :Object(anObject)
return Type: boolean*/


class Yash{
	
	public static void main(String args[]){
		
		String str1 = "Yash";
		String str2 = new String("Yash");

		System.out.println(str1.equals(str2));
	}
}
