/*Method : public char chsrAt(int index)
 *
  - it returns the character located at specified index within the given string

   parameter: integer(index)
   return type : character*/

class Yash{
	
	public static void main(String args[]){
		String str = "Yash Kirad";

		System.out.println(str.charAt(0));
		System.out.println(str.charAt(8));
		System.out.println(str.charAt(6));
		System.out.println(str.charAt(5));
	}
}
