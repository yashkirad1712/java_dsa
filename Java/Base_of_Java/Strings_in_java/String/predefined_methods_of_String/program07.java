/*Method : public boolean equalsIgnoreCase(String another String);
 *
Description : compares a string to this string ignoring case
Parameter : String(str2)
Return Type : Boolean*/

class Yash{
	
	public static void main(String args[]){
		
		String str1 = "YashKirad";
		String str2 = "yashkired";

		System.out.println(str1.equalsIgnoreCase(str2));
	}
}
