/*Method : public String Substring(int index)
 *
Description : Creates a substring of the given String starting at a specified index & ending at the end of given String
Parameter : Integer
Return Type : String*/

class Yash{
	
	public static void main(String args[]){
		
		String str = "Core2Web Tech";

		System.out.println(str.substring(5));
		System.out.println(str.substring(0,3)); // [includes start index ,Excludes Last index);
	}
}
