/*Method : public int CompareToIgnoreCase(string str)
 *
 - It compares str1  and str2 
 - if str1 is capital then str2 is small 
 -case sensitive
parameter : String
return type : Integer*/

class Yash{

	public static void main(String [] args){
		
		String str1 = "YASHKIRAD";
		String str2 = " ";

		System.out.println(str1.compareToIgnoreCase(str2));
	}
}
