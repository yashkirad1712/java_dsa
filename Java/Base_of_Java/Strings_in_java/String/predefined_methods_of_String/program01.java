/*method : public string concat(String str)
 *
 
 -concatinate string the this string i.e Another string is concatinated with the first string
 -Implements new Array of character whose length is sum of str1.lenght and str2.length
 
parameter: String
return type : String*/

class Yash{
	
	public static void main(String[] args){
		String str1 = "Core2";
		String str2 = "Web";
		String str3 = str1.concat(str2);

		//here str1 is : this pointer.

		System.out.println(str3);
	}
}
