/*In a Static method we can only access : Static methods ,Variables etc.*/
/*In a Non Static Method we can access : Static as well as Non static Methods and Variables etc*/

import java.io.*;

class Yash{
	static int x = 10;
	int y = 20;
	
	void fun(){
		System.out.println("In Fun");
		System.out.println("x = "+x);
		System.out.println("y = "+y);
		gun();
		
	}
	
	static void gun(){
		System.out.println("In gun");

		System.out.println("x = "+x);
		main(new String[] {});
	
	}

	public static void main(String args[]){
		System.out.println("In Main");
		
		Yash obj = new Yash();

		System.out.println("Calling Fun");
		obj.fun();
		
	}
}
