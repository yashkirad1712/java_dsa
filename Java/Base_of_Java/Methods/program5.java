/*Passing parameter to the methods*/

import java.util.*;

class Yash{
	static void add(int a,int b){
		System.out.println("Addition of "+a+" & "+b+" is :"+(a+b));
	}
	
	static void sub(int a,int b){
		System.out.println("Subtraction of "+a+" & "+b+" is :"+(a-b));
	}
	
	static void mult(int a,int b){
		System.out.println("Multiplication of "+a+" & "+b+" is :"+(a*b));
	}
	
	static void div(int a,int b){
		System.out.println("Division of "+a+" & "+b+" is :"+(a/b));
	}


	public static void main(String args[]){
		System.out.println("Enetr the Elements:");
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		
		System.out.println("Calling Addition method");
		add(a,b);
		
		System.out.println("Calling Subtraction method");
		sub(a,b);
		
		System.out.println("Calling Multiplication method");
		mult(a,b);
		
		System.out.println("Calling Divison method");
		div(a,b);
	
	}
}
