/*Methods in Java : there are 2 types of methods 1.Static and 2.Non-Static*/
/*Static have a keyword static with them where Non-Static dont have Static keyword*/
/*Static method can be called directly from the Static method wheras NonStatic method cannot be called from a Static Context*/

class Yash{
	public static void main(String args[]){
		fun();
		gun();
	}

	static void fun(){
		System.out.println("In Fun Method");
	}

	void gun(){
		System.out.println("In Gun Method");	
	}
}
