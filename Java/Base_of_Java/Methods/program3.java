/*Accesing the non static method and variable throught a static context*/

class Yash{

	static int x = 10;
	int y = 20;
	
	static void gun(){
		System.out.println("In gun");
	}

	void fun(){
		System.out.println("In fun");
	}

	public static void main(String args[]){
		
		System.out.println("Start Main");
		System.out.println(x);
		
		Yash obj = new Yash();
		
		System.out.println(obj.y);

		gun();
		obj.fun();
		System.out.println("End Main");

	}
}
