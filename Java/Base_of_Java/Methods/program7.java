/*JAVA compiler checks  things:
 1.Syntax of the code
 2.java langugae specific things
i.e: for the parameters there must not any sort of data loss */

class Yash{
	public static void main(String args[]){
		System.out.println("In main");
		Yash obj = new Yash();
		obj.fun(10);
		obj.fun(15.5f);
		obj.fun('A');
		//boolean needs boolean as a parameter no other parameters are required
		System.out.println("End main");

	}

	void fun(float x){
		
		System.out.println("In Fun");
		System.out.println(x);
	}
}
