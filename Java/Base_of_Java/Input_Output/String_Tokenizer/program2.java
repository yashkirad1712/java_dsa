/*String Tokenization at run time */

import java.util.*;
import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Destination,Car Number,Grade,Fair Price");
		String OlaInf = br.readLine();
		
		System.out.println("The string Enetred by the user is: ");
		System.out.println(OlaInf);

		StringTokenizer obj = new StringTokenizer(OlaInf,"/");
		
		//breaking the OlaInf into tokens with "/" as a deliminator.
		
		while(obj.hasMoreTokens()){
			String Token = obj.nextToken();
			System.out.println(Token);
		}
		/*
		System.out.println(Token1);
		System.out.println(Token2);
		System.out.println(Token3);
		System.out.println(Token4);
   	
		System.out.println("Typecasting Of the tokens");
		int CarNo = Integer.parseInt(Token2);
		char Grade = Token3.charAt(0);
		double Fair = Double.parseDouble(Token4);
	
		Fair = Fair + 8.37;
		
		System.out.println("After Typecasting:");

		System.out.println(Token1);
		System.out.println(CarNo);
		System.out.println(Grade);
		System.out.println("The fair + GST is : " + Fair + " Rupees /-");
		*/
	}
}
