/*Using hasMoreTokens() with scanner method*/

import java.util.*;

class Yash{
	public static void main(String args[]){
		System.out.println("Enter a String:");
		
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		
		System.out.println("The Given String is:");
		System.out.println(str);
		
		StringTokenizer st = new StringTokenizer(str,"/");
		
		while(st.hasMoreTokens()){
			System.out.println(st.nextToken());
		}
	}
}
