/*Using different dataypes with scanner classes*/

import java.util.Scanner;

class Yash{
	
	public static void main(String args[]){
		
		Scanner obj = new Scanner(System.in);
		
		System.out.println("Enter your name");
		String name = obj.next();
		
		System.out.println("Enter your Age");
		int age = obj.nextInt();
		
		System.out.println("Enter your Initial");
		char ini = obj.next().charAt(0);
		
		System.out.println("Enter your Salary");
		float salary = obj.nextFloat();
	}
}
