/*Taking character as a input*/

import java.io.*;

class Demo{
	
	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter the character");
		char ch = (char)br.read();
		br.skip(1);	

		System.out.println("Enter the String");
		String str = br.readLine();
		
		System.out.println("The character is: " + ch);
		System.out.println("The String is: " + str);

		int[] arr = new int[4];//{3,4,5,6,7};
	}	
}
