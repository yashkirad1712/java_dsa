/*Using buffered close in the code and checking whether it closes the input from the keyboard with jvm or that specific buffered object with the input of the process */

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		   BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
			
		   System.out.println("Enter the number");
		   int number1 = Integer.parseInt(br1.readLine());
		   
		   br1.close();

		   BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
			
		   System.out.println("Enter the number");
		   int number2 = Integer.parseInt(br2.readLine());


	}
}
