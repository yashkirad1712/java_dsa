/*Basic datatypes in java*/

class Datatypes{
	public static void main(String args[] ){
		int x = 10;
		float y = 1.5f;
		char z = 'A';
		double d = 15.0364;
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(d);
	}
}
