/*Java new concept for initializing the varaibles with the same name between the same scope*/

class Yash{
	public static void main(String args[]){
		int x =10;

		{
			int x = 15;
			System.out.println(x);
		}
		{
			int x = 20;
			System.out.println(x);
		}
		System.out.println(x);
	}
}
/*
 program3.java:8: error: variable x is already defined in method main(String[])
                         int x = 15;
 program3.java:12: error: variable x is already defined in method main(String[])
 			 int x = 20;
									                            
	2 errors
 */
