/*Logical operator*/

class Program17{
	public static void main(String args[]){
		int x=10;
		int y=20;
		
		System.out.println(x<y && y>x);
		
		
		/*int ans = x && y;
		 * will give erroe because it is a bad operand for binary operator
		 * int ans = x<y && y>x
		 * will give erroe because we are trying to stroe the boolean value into the integer variable
		 */
	}
}
