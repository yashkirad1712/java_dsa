/*WAP to print the Sum of even number and multiplication of odd*/

class Program14{
	public static void main(String[] args){
		int n=10;
		int sum =0;
		int multi =1;
		while(n!=0){
			if(n%2==0){
				sum = sum+n;
			}else{
				multi = multi * n;
			}
			n--;
		}
		System.out.println("Sum of even numbers is "+sum);
		System.out.println("Multiplicatipon of odd numbers is "+multi);
	}
}
