/*Take an integer as a input N
 * print the multiples of 4 till N*/

class Program4{
	public static void main(String[] args){
		int N = 22;
		int i = 4;

		while(i <= N){
			System.out.println(i);
			i += 4;
		}
	}
}
