/*While Loop*/

/*Print the numbers from 10 to 1 in while loop*/

class Program2{
	public static void main(String[] args){
		int i = 10;
		while(i != 0){
			System.out.println(i);
			i--;
		}
	}
}
