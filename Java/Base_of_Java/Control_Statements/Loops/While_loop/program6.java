/*Factorial of a number 
 *
 * input 5 = 5*4*3*2*1 = 120*/

class Program6{
	public static void main(String args[]){
		int N = 5;
		int fact = 1;
		if(N == 0){
			System.out.println(1);
		}else{
			while(N != 1){
				fact = fact * N;
				N--;
			}
			System.out.println(fact);
		}	
	}
}
