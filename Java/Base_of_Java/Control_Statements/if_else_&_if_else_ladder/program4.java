/*By taking the temperature as a input check whether the person is having low,Normal or high temperature*/
/*
 * high  > 98.6
 * normal 98.0<= && >=98.6
 * low 98.0 <*/
class Program4{
	public static void main(String args[]){
		double temp = 98.9;

		if(temp <= 98.0 && temp >=98.6){
			System.out.println("Normal Temperature");
		}else if(temp > 98.6){
			System.out.println("High Temperature");
		}else{
			System.out.println("Low Temperature");
		}
	}

}
