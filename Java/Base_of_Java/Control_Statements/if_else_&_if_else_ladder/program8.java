/*Electrcity Bill problem*
 *
 * if unit <= 100 then the cost of the bill is 1 per unit
 * if unit > 100 then the cost of the bill is 2 per unit
 * ex: unit is 99
 * bill is 99
 *
 * unit is 100
 * bill is 100 
 *
 * unit is 200
 * bill is 300*/

class Program8{
	public static void main(String args[]){
		int unit = 200;

		if(unit <= 100){
			System.out.println("The bill is " + unit + " Rupess");
		}else{
			System.out.println("The bill is " + ((unit-100)*2 + 100) + " Rupess");
		}
	}
}

