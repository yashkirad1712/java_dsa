/*Using integer array to print as Float in For each loop*/

class Yash{
	
	public static void main(String args[]){
		
		int[] arr = {10,20,30,40,50};

		System.out.println("Using For Each Loop");

		for(float x :arr){
			System.out.println(arr[0]);
			System.out.println(x);
		}
	}
}
