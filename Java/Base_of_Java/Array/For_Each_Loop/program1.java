/*Working of For each Loop: it works on the object of the array and print the objects, 
 * 1.we cannot manipulate the flow of for each loop*/

class Yash{

	public static void main(String args[]){
		
		int[] arr = {10,20,30,40,50};

		System.out.println("Using For each loop for element printing");
		for(int var : arr){
			System.out.println(var + 1);
		}
	}
}
