/*WAP to take size of array from user and also take integer elements from user Print sum
  of odd elements only
  input : Enter size : 5
  Enter array elements : 1 2 3 4 5
  output = 9*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		System.out.println("Enter the size of the Array");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter the Elements of the Array");
		
		int sum = 0;

		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 1){
				sum = sum + arr[i];
			}
		}

		System.out.println("The sum of Odd elements of the Array is : " +sum );

	}
}
