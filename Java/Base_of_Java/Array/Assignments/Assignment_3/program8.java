/*WAP to find the ArmStrong number form the array and return its index*/

import java.io.*;

class Yash{

	int ArmStrong(int[] arr){
		
		for(int i= 0 ;i<arr.length ;i++){
			
			int val = arr[i];
			int len = 0;
			while(val != 0){
				len++;
				val = val/10;
			}
			int val1 = arr[i];
			int sum = 0;
			while(val1 != 0){
				int mult = 1;
				for(int j=1;j<=len;j++ ){
					mult = mult* (val1 % 10); 
				}
				sum = sum+mult;
				val1 = val1/10;
			}
			if(sum == arr[i]){
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];
		System.out.println("Enter the elements in the array");

		for(int i=0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.ArmStrong(arr);

		if(ret != -1){
			System.out.println("ArmStrong Number " + arr[ret] + " founded at index " + ret);
		}else{
			System.out.println("No ArmStrong number found");
		}
	}
}

