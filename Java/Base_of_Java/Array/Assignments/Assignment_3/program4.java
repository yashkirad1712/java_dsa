/*WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5*/


import java.io.*;

class Yash{
	
	 int  PrimeEle(int[] arr){
		 int val = 0;
		for(int i=0 ;i<arr.length;i++){
			int count = 0;
			for(int j=1 ;j*j <= arr[i];j++){
				if(arr[i] %j == 0){
					count =count+ 2;
				}
			}
			
			if(count == 2){
				System.out.println(arr[i]);
				val = i;
			}
		}
		return val;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the Size ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in the array");
		for(int i=0 ;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		Yash obj = new Yash();
		int ret = obj.PrimeEle(arr);
		System.out.println("The Element " + arr[ret] + "is Prime and is present at index " + ret);
	}
}
