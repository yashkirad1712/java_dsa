/*WAP to print the second smallest element from the array*/

import java.io.*;

class Yash{

	int SecondSmall(int[] arr){
			int min1 = arr[0];
			int min2 = arr[0];
			for(int i=1 ;i<arr.length ;i++){
				if(min1 > arr[i]){
					min1 = arr[i];
				}
			}
			
			for(int j=0 ;j<arr.length;j++){
				if(min2 > arr[j] && arr[j] != min1){
					min2 = arr[j];
				}
			}	

			return min2;
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];
		System.out.println("Enter the elements in the array");

		for(int i=0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.SecondSmall(arr);

		System.out.println("Second Smallest  Number is " + ret);
		
	}
}

