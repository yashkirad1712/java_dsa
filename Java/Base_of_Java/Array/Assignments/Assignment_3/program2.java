/*WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465*/

import java.io.*;

class Yash{

	void ReverseEle(int[] arr){
		
		System.out.println("The number of Digits in the elements of the array is:");
		for(int i=0 ;i<arr.length ;i++){
			int rev = 0;;
			for(int j =arr[i] ;j >0 ;j=j/10){
				rev= rev*10 + j%10;
			}
			System.out.println(arr[i] + " : " + rev);

		}
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));

		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in the array");
		for(int i=0 ;i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj= new Yash();

		obj.ReverseEle(arr);
	}
}
