/*WAP to find a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4*/

import java.io.*;

class Yash{
	
	void CompoEle(int[] arr){
		
		for(int i=0 ;i<arr.length;i++){
			for(int j = 2 ;j*j <= arr[i];j++){
				if(arr[i] % j == 0){
					System.out.println(arr[i] +" Element at index "+ i + " is composite");
					break;
				}
			}
		}
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements in the array");
		for(int i=0 ;i<size ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		obj.CompoEle(arr);
	}
}
