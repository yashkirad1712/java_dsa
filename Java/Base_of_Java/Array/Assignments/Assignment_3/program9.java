/*WAP to print the second largest element from the array*/

import java.io.*;

class Yash{

	int SecondLarge(int[] arr){
			int max1 = arr[0];
			int max2 = 0;
			for(int i=0 ;i<arr.length ;i++){
				if(max1 < arr[i]){
					max2 = max1;
					max1 = arr[i];
				}
			}
			if(max2 == 0){
				for(int i=1;i<arr.length;i++){
					if(max2 < arr[i]){
						max2 = arr[i];
					}
				}
			}

			return max2;
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];
		System.out.println("Enter the elements in the array");

		for(int i=0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.SecondLarge(arr);

		System.out.println("Second Largest  Number is " + ret);
		
	}
}

