/*WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3*/


import java.io.*;

class Yash{
	
	int PerfectNo(int[] arr){
		for(int i=0 ;i<arr.length ;i++){
			int sum = 0;
			for(int j=1 ;j*j <= arr[i];j++){
				if(arr[i] % j == 0){
					sum = sum + j + arr[i]/j;
				}
			}
			sum = sum - arr[i];
			if(sum == arr[i]){
				return i;
			}
		}
		return -1;
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of the Array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in the array");
		for(int i=0 ;i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.PerfectNo(arr);
		if(ret != -1){
			System.out.println("perfect no. "+arr[ret]+ " foun at index "+ret);
		}else{
			System.out.println("No perfect number is present");
		}	

	}
}
