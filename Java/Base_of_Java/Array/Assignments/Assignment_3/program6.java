/*WAP to find the pallindrome number form the array and return its index*/

import java.io.*;

class Yash{

	int Pallindrome(int[] arr){
		
		for(int i=0 ;i<arr.length ;i++){
			int val = 0;
			for(int j=arr[i] ;j> 0;j = j/10){
				val = val*10 + j%10;
			}

			if(val == arr[i]){
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];
		System.out.println("Enter the elements in the array");

		for(int i=0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.Pallindrome(arr);

		if(ret != -1){
			System.out.println("Pallindrome Number " + arr[ret] + " founded at index " + ret);
		}else{
			System.out.println("No Pallindrome number found");
		}
	}
}

