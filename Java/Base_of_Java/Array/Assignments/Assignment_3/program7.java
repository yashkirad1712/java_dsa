/*WAP to find the Strong number form the array and return its index*/

import java.io.*;

class Yash{

	int Strong(int[] arr){
		
		for(int i=0 ;i<arr.length ;i++){
			int val = arr[i];
			int sum = 0;
			while(val != 0){
				int mult = 1;
				for(int j=val%10 ;j >0;j--){
					mult = mult*j; 	
				}
				sum = sum + mult;
				val = val/10;
			}

			if(sum == arr[i]){
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];
		System.out.println("Enter the elements in the array");

		for(int i=0; i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		int ret = obj.Strong(arr);

		if(ret != -1){
			System.out.println("Strong Number " + arr[ret] + " founded at index " + ret);
		}else{
			System.out.println("No Strong number found");
		}
	}
}

