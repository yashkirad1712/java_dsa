/*Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4*/

import java.io.*;

class Yash{

	void CountEle(int[] arr){
		
		System.out.println("The number of Digits in the elements of the array is:");
		for(int i=0 ;i<arr.length ;i++){
			int count = 0;;
			for(int j =arr[i] ;j >0 ;j=j/10){
				count++;
			}
			System.out.println(arr[i] + " : " + count + " - digits");

		}
	}
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));

		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in the array");
		for(int i=0 ;i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj= new Yash();

		obj.CountEle(arr);
	}
}
