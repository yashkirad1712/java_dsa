/*Wap to find the minimum element from the array*/

import java.io.*;

class Yash{

	int MinEle(int arr[]){
		
		int min = arr[0];

		for(int i=1 ;i<arr.length;i++){
			if(arr[i] < min)
				min = arr[i];
		}

		return min;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements of the array");

		for(int i=0 ;i<arr.length;i++){
		
			arr[i]= Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		System.out.println("The minimum element in the array is :" + obj.MinEle(arr));


	}
}
