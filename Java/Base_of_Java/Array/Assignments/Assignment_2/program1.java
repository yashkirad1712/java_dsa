/*Write a program to create an array of ‘n’ integer elements.
 * Where ‘n’ value should be taken from the user.
 * Insert the values from users and find the sum of all elements in the array.
 * Input:
 * n=6
 * Enter elements in the array: 2 3 6 9 5 1
 * Output:
 * 26*/

import java.io.*;

class Yash{
	
	int SumArr(int arr[]){
		
		int sum = 0;
		for(int i=0 ;i<arr.length;i++)
			sum= sum + arr[i];

		return sum;
	}

	public static void main(String args[])throws IOException{
	
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the Elements of the array");
		for(int i=0 ;i<arr.length;i++)
			arr[i] = Integer.parseInt(br.readLine());

		Yash obj = new Yash();
		System.out.println("The Sum of the elements of the array is : " + obj.SumArr(arr));

	}
}
