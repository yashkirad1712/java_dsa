/*wap to print the elements whose sum is even*/

import java.io.*;

class Yash{
	
	void evenEle(int arr[]){
		System.out.println("The Required Number are: ");
		for(int i=0 ;i<arr.length ;i++){
			int val = arr[i];
			int sum =0;

			while(val != 0){
				sum = sum + val%10;
				val = val/10;
			}
			if(sum % 2 == 0){
				System.out.println(arr[i]);
			}
		}
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enetr the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enetr the Elements of the array");
		for(int i =0 ;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		Yash obj = new Yash();
		obj.evenEle(arr);

	}
}
