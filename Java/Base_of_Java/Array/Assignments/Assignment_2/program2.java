/*WAP to find the number of even and odd integers in a given array of integers
 * Input: 1 2 5 4 6 7 8
 * Output:
 * Number of Even Elements: 4
 * Number of Odd Elements : 3*/

import java.io.*;

class Yash{

	int CountNum(int arr[]){
		int count = 0;
		for(int i =0;i<arr.length;i++){
			if(arr[i] % 2 == 0)
				count ++;
		}

		return count;	
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enetr the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the number of the array elements");
		for(int i =0 ;i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		Yash obj = new Yash();
		int ret = obj.CountNum(arr);
		
		System.out.println("The count of Even number is :" + ret);
		System.out.println("The count of odd number is :" + (arr.length - ret));


	}
}
