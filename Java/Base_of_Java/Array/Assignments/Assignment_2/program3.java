
/*Sum of all odd and even numbers*/

import java.io.*;

class Yash{

	void  CountNum(int arr[]){
		int sum1 = 0;
		int sum2 = 0;

		for(int i =0;i<arr.length;i++){
			if(arr[i] % 2 == 0)
				sum1 = sum1 +arr[i];
			else{
				sum2 = sum2 + arr[i];
			}	
				
		}

		System.out.println("The sum of the even :" + sum1);
		System.out.println("The sum of the odd :" + sum2);

	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enetr the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the number of the array elements");
		for(int i =0 ;i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		Yash obj = new Yash();
		obj.CountNum(arr);
		

	}
}
