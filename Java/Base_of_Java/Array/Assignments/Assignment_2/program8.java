/*WAP to find the Uncommon elements present in two different Array*/

import java.io.*;

class Yash{
	
	 void uncomEle(int arr1[],int arr2[]){
		System.out.println("The Uncommon Elements are:");
		
		for(int i=0 ;i< arr1.length ;i++){
			for(int j = 0;j< arr2.length;j++){
				if(arr1[i] == arr2[j]){
					arr1[i] = '-';
					arr2[j] = '-';
				}
			}
		}
		
		for(int i=0 ;i<arr1.length ;i++){
			if(arr1[i] != '-'){
				System.out.println(arr1[i]);
			}	
		}

		for(int i=0 ;i<arr2.length ;i++){
			if(arr2[i] != '-'){
				System.out.println(arr2[i]);
			}	
		}		
	}

	public static void main(String args[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char ch;
		
		System.out.println("Enetr the size of the 1 array");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1]; 
							 
		System.out.println("Enetr the size of the 2 array");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[size2];

			
	do{	
		System.out.println("| 1.Fill Data in 1 array   |");
		System.out.println("| 2.Fill Data in 2 array   |");
		System.out.println("| 3.Find UnCommon Element  |");
		System.out.println("Enter your Choice");
		int num = Integer.parseInt(br.readLine());
		
		
		switch(num){
		
				case 1:
					{
						System.out.println("Enetr the elements in the " + num + " Array");
						for(int i=0 ;i<arr1.length;i++){
						arr1[i] = Integer.parseInt(br.readLine());
						}		
					}
					break;
	
				case 2:
					{
						System.out.println("Enetr the elements in the " + num + " Array");
						for(int i=0 ;i<arr2.length;i++){
							arr2[i] = Integer.parseInt(br.readLine());
						}
					}	
					break;

				case 3 :
					{
						System.out.println("Calling The UncommonElement Method.....");
						Yash obj = new Yash();
						obj.uncomEle(arr1,arr2);
					}
					break;

				default :
						System.out.println("Invalid Number");
					
			}
		System.out.println("Do you want to continue: ");
		ch = (char)br.read();
		br.skip(1);

		}while(ch == 'y' || ch == 'Y');
		
  	}	
}	


