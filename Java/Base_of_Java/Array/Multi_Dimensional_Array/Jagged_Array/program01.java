/*JAgged Array : 1.It is a Array whic is created at run time by the convineince of the user.
 * 		 2.It is used for memeory management 
 * 		 3.there are two types of initializing the Jagged Array
 *
 * 		 Part 1 : Initialization of jagged array with  hardcoded values*/

class Yash{

	public static void main(String args[]){
		
		int[][] arr = {{1,2,3},{4,5,6},{7,8,9}};
		int[][] arr1 = {{11,12,13},{14,15},{16}};

		System.out.println("Printing the Array arr with Matrix of 3x3");
		for(int x[] :arr ){
			for(int y : x) System.out.print(y + " ");

			System.out.println();
		}
		
		System.out.println("Printing the Array arr1 with no fixed Coloumns");
		for(int x[] :arr1 ){
			for(int y : x) System.out.print(y + " ");

			System.out.println();
		}

	}
}
