/*Part - 2 : Initializing the Jagged Array with USer Values*/

import java.io.*;

class Yash{
	
	public static void main(String args[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows");
		int row = Integer.parseInt(br.readLine());

		//Initializes the 2-D array
		int [][] arr = new int[row][];

		
		System.out.println("Enter the Number of coloumns in 1st Row");
		int col1 = Integer.parseInt(br.readLine());

		System.out.println("Enter the Number of coloumns in 2nd Row");
		int col2 = Integer.parseInt(br.readLine());

		System.out.println("Enter the Number of coloumns in 3rd Row");
		int col3 = Integer.parseInt(br.readLine());

		//Creating the Object of 1D array Manually cauz it is not provided with the initailizer list
		arr[0] = new int[col1];
		arr[1] = new int[col2];
		arr[2] = new int[col3];

		System.out.println("Enter the elements in the array");
		for(int[] x : arr){
			
			for(int y=0;y<x.length;y++) x[y] = Integer.parseInt(br.readLine());
		}

		System.out.println("The array So formed is: ");

		for(int x[] : arr){
		
			for(int y :x) System.out.print(y + " ");

			System.out.println();
		}

	}
}
