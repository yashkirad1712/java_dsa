/*Basic structure of 2-D array*/

import java.io.*;
class Yash{
	
	public static void main(String args[])throws IOException{
		
		int [][] arr = new int[2][2];
		// row is mandatory
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enetr the number of the rows");
		int row = Integer.parseInt(br.readLine());

		for(int i=0 ;i<row;i++){
			for(int j = 0;j <2;j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int x[] : arr){
			System.out.println(x);
		}
	}
}
