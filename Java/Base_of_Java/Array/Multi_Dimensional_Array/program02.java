/*Right way of initialization of multi dimensional array*/


class Yash{

	public static void main(String args[]){
		
		int [][] arr = new int[5][];
		int [][] arr1 = new int[5][3];
		
		/*error: array dimension missing*/
		//int [][] arr2 = new int[][];
		

		System.out.println(arr);
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr1[2][2]);

		/*Exception in thread "main" java.lang.NullPointerException: Cannot load from int array because "<local1>[2]" is null*/
		/*if we do not assign the coloumns and still try to acess them then we get the above Exception*/
		//System.out.println(arr[2][2]);
	}
}
