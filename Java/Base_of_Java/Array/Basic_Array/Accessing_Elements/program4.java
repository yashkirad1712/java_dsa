/*Taking Input from an User and Accessing Array Elements and printing it */

/*NOTE: IN java Integer array cannot store any other value of any other datatypes,in it not even char as it is gone as a strin in it*/
import java.io.*;

class Yash{
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of the Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the elements into the Array");
		for(int i=0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("The elements of the Array are:");
		for(int j=0 ; j<size ; j++){
			System.out.println("|_" + arr[j] + "_|");
			if(j != (size-1)){
				System.out.println("  | ");
			}	
		}
	}
}
