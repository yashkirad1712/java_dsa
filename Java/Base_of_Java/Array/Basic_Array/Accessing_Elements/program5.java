/*Different types of arrays having default values as:*/


class Yash{
	public static void main(String args[]){
		int iarr[] = new int[4];

		char carr[] = new char[4];

		double darr[] = new double[4];

		float farr[] = new float[4];

		String sarr[] = new String[4];

		boolean barr[] = new boolean[4];

		System.out.println("Integer Array");
		for(int i=0 ;i<4;i++){
			System.out.println(iarr[i]);
		}
		
		System.out.println("Character Array");
		for(int j=0 ;j<4;j++){
			System.out.println(carr[j]);
		}
	
		System.out.println("Double Array");
		for(int k=0 ;k<4;k++){
			System.out.println(darr[k]);
		}
	
		System.out.println("Float Array");
		for(int l=0 ;l<4;l++){
			System.out.println(farr[l]);
		}
	
		System.out.println("String Array");
		for(int m=0 ;m<4;m++){
			System.out.println(sarr[m]);
		}

		System.out.println("Boolean Array");
		for(int n=0 ;n<4;n++){
			System.out.println(barr[n]);
		}
	
	}
}
