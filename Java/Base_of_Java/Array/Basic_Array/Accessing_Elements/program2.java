/*Dimesion of an array(size)*/

class Yash{
	public static void main(String args[]){
		
		int arr1[];   //accepted in java
		
		int arr2[] = new int[2];  //here 2 is the dimension of the array if we dont give the dimension then it will give error
		
		/*
		 ----IN C language ------
			#include<stdio.h>

			void main(){
				int arr[];
				//It will give error:array size missing
			}
		 */
	
	}
}
