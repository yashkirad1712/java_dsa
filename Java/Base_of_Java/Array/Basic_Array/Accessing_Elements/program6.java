/*Print the Sum of array elements*/

import java.io.*;

class Yash{
	
	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int [size];

		System.out.println("Enter the Elements of the array");

		int sum =0;

		for(int i=0 ;i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
			sum= sum + arr[i];
		}
		
		System.out.println("The sum of the Array is : " + sum);
	}
}
