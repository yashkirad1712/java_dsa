/*Ways of writing array syntax In java*/


class Yash{
	public static void main(String args[]){
		int arr1[] = new int[3];
		arr1[0] = 10;
		arr1[1] = 10;
		arr1[2] = 10;

		int arr2[] = new int[]{20,20,20,20};

		int arr3[] = {30,30,30,30,30,30};
		
		//this way of initializing an array is wrong;
		//we can only provide initializer list or dimension to an array but not both;
		//int arr4[] = new int[3]{40,40,40}; /*there is one error*/
	}
}
