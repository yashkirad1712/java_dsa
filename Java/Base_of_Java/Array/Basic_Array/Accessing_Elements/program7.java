/*Return the count of the Even and Odd elements*/

import java.io.*;

class Yash{

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size of the array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int [size];


		System.out.println("Enter the Elements of the array");
		
		int count = 0;

		for(int i=0 ;i<arr.length ;i++){
			arr[i] = Integer.parseInt(br.readLine());
		
			if(arr[i] % 2 == 0) count ++;
		}	
		

		System.out.println("The Even count is: " + count);
		System.out.println("The Odd count is: " + (arr.length - count));
	}
}
