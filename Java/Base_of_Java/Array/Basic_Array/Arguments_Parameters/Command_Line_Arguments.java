/*Command Line Arguments :1. These are the String which are given to the main methods String array at runtime.
 * 			  2. Every object of this String array has a new Unique id as it is created at runtime*/


class Yash{
	
	public static void main(String args[]){
		
		System.out.println("Printing the command line arguments: ");
		for(String x : args){
			System.out.println(x);
		}
	}
}
