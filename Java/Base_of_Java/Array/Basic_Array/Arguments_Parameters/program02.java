/*Proving that Array address is sent to the Method : As change in the method in Array is reflected in the Main Method array*/

class Yash{
	
	void fun(int [] arr){
		System.out.println("The elements are ..");
		for(int x : arr){
			System.out.println(x);
		}
		
		System.out.println("Modification of elements in Fun");
		for(int i=0 ;i<arr.length;i++){
			arr[i] = arr[i] + 50;
		}	
	}

	public static void main(String args[]){
		
		int [] arr = {10,20,30,40,50};

		System.out.println("The array so formed is: ");
		for(int x : arr){
			System.out.println(x);
		}

		Yash obj = new Yash();
		obj.fun(arr);
		
		System.out.println("Modification seen in Main too..");
		for(int y : arr){
			System.out.println(y);
		}
	}


}
