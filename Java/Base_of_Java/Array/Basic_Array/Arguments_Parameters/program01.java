/*Passing array as a Argument*/

class Yash{
	
	void fun(int [] arr){
		
		System.out.println("The Array which is passed is:" );

		for(int x :arr){
			System.out.println(x);
		}
	}

	public static void main(String args[]){
		
		int [] arr ={10,20,30,40,50};

		Yash obj = new Yash();
		obj.fun(arr);
	}
}
