/*Showing that Elements of the array are stored in the Integer Cache as a object and do follow the Integer Cache rules*/


class Yash{
	
	void fun(int[] arr){
		arr[2] =70;
		arr[3] =80;

		System.out.println("Modifying Some elements in the array");
		System.out.println(arr[2] +" = "+System.identityHashCode(arr[2]));
		System.out.println(arr[3] +" = "+System.identityHashCode(arr[3]));
	} 

	public static void main(String args[]){
		
		int [] arr={10,20,30,40,50};

		Yash obj = new Yash();
		System.out.println("Elements are:");
		for(int w : arr){
			System.out.println(w);
		}
		System.out.println("Printing the Identity HashCodes of the elements of the array");
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));
		System.out.println(System.identityHashCode(arr[4]));

		obj.fun(arr);
		System.out.println("After modification Array becomes: ");
		for(int w : arr){
			System.out.println(w);
		}

		int x = 70,y = 80;

		System.out.println("Printimg some variables having same values as of some elements in the array :");
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));

	}
}
