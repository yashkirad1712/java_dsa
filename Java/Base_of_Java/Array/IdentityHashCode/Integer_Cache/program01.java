/*Integer cache is the pool where objects are stored between the range of -128 to 127 and given same object and rest all objects are given a new area*/

class Yash{
	
	public static void main(String args[]){
		
		//x will become a object internally and will get a unique id
		int x = 10;
		
		//y will also become a object and as y and x have same values between the range of -128 to 127 unique id will be same
		int y = 10;

		//an Class will also have the same id as it also goes like an object internally
		Integer z = 10;

		//but when we say new a new object is created so its object will have different id
		Integer a = new Integer(10);
		
		//warning: [removal] Integer(int) in Integer has been deprecated and marked for removal
		
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));
		System.out.println(System.identityHashCode(a));
	}
}
