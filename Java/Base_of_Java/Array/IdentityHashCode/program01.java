/*Proving that Array internally has a pointer and everything inside the array is refrenced with the object i.e known as pointers in the C*/

class Yash{
	
	public static void main(String args[]){
		
		int arr[] = new int[3];
		char ch[] = new char[3];
		float farr[] = new float[3];
		double darr[] = new double[3];
		boolean barr[] = new boolean[3];
		String sarr[] = new String[3];

		System.out.println("Enter the elements in the array");

		for(int i=0 ;i<arr.length;i++){
			
			arr[i] = i;
			ch[i] = 'A';
			farr[i] = i;
			darr[i] = i;
			barr[i] = true;
			sarr[i] = "| Y.K |";

		}


		System.out.println(arr + " -integer array");
		System.out.println(ch + " -character array");
		System.out.println(farr + " -Float array");
		System.out.println(darr + " -Double Array");
		System.out.println(barr + " -Boolean array");
		System.out.println(sarr + " -String array");

		char [] arr1 = {'A' ,'B','C'};
		System.out.println(arr1);

	}
}
