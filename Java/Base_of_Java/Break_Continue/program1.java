/*Break And Continue*/

class Yash{
	public static void main(String args[]){
		int n = 15;
		int count = 0;

		for(int i=1;i*i<=n;i++){
			if(n%i == 0){
				count += 2;
			}
			if(count > 2){
				break;
			}
		
		}
		if(count <= 2){
			System.out.println("Prime Number");
		}else{
			System.out.println("Not a Prime Number");
		}
	}
}
