/*More conditions in a loop in if statemnts*/

class Yash{
	public static void main(String args[]){
		int n=3*5;

		for(int i=1;i<=50;i++){
			if((i%3 ==0 && i%5 == 0) || i%4 == 0){
				continue;
			}
			System.out.println(i);
		}
	}
}
