/*Input : String
 * To find: Largest Ascii Character*/


import java.io.*;

class Yash{
	
	static char LargesstAscii(String str){
		
		char[] ch = str.toCharArray();
		
		int max = -1;

		for(int i=0 ;i<ch.length;i++){
			int val = ch[i];

			if(val > max){
				max = i;
			}

		}
		
		return ch[max];
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();
		
		System.out.println(LargestAscii(str));
	}
}
