/*Input : String
 * To find: Toggle the string*/


import java.io.*;

class Yash{
	
	static String ToggleStr(String str){
		
		char[] ch = str.toCharArray();
		
		for(int i=0 ;i<ch.length;i++){
			int val = ch[i];

			if(val >= 65 && val<= 90){
				ch[i] = (char)(val + 32);
			}
			
			if(val >= 97 && val<= 122){
				ch[i] = (char)(val - 32);
			}
		}
		
		String ret = new String(ch);
		return ret;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();
		
		System.out.println(ToggleStr(str));
	}
}
