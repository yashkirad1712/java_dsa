/*Input : String
 * To find: Anagram String*/


import java.io.*;

class Yash{
	
	static int AnagramStr(String str1,String str2){
		
		char[] ch1 = str1.toCharArray();
		char[] ch2 = str2.toCharArray();
		
		if(ch1.length != ch2.length){
			return 0;
		}

		for(int i=0 ;i<ch1.length;i++){
			for(int j=0 ;j<ch2.length;j++){
				if(ch1[i] == ch2[j]){
					ch2[j] = 0;
				}
			}	
		}

		for(int k=0 ;k<ch2.length;k++){
			if(ch2[k] != 0){
				return 0;
			}
		}

		return 1;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String one");
		String str1 = br.readLine();

		System.out.println("Enter the String two");
		String str2 = br.readLine();
		
		
		int ret = AnagramStr(str1,str2);

		if(ret == 1){
			System.out.println("Strings are Anagram");
		}else{
			System.out.println("Strings are not Anagram");
		}
	}
}
