/*Input : String
 * To find: AlphNumeric String*/


import java.io.*;

class Yash{
	
	static int AlphanumericStr(String str){
		
		char[] ch = str.toCharArray();
		
		int flag1 = 0;
		int flag2 = 0;

		for(int i=0 ;i<ch.length;i++){
			int val = ch[i];

			if(val >= 49 && val <=57){
				flag1 = 1;
			}

			if((val >= 65 && val <= 90) || (val >=97 && val <=122)){
				flag2 = 1;
			}

		}

		if(flag1 == 1 && flag2 == 1){
			return 1;
		}
		
		return 0;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();
		
		int ret = AlphanumericStr(str);

		if(ret == 1){
			System.out.println("String is Alphanumeric");
		}else{
			System.out.println("String is not Alphanumeric");
		}
	}
}
