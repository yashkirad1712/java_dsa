/*Input : String
 * To find: Vowel present in the string*/


import java.io.*;

class Yash{
	
	static int VowelCount(String str){
		
		char[] ch = str.toCharArray();
		
		char val = 97;
		int count = 0;
	

		for(int i=0 ;i<ch.length;i++){
			if(ch[i] == val || ch[i] == (char)(val+4) || ch[i] == (char)(val+8) || ch[i] == (char)(val+14) || ch[i] == (char)(val+20)){
				count++;
			}	
			
			if(ch[i] == (char)(val-32) || ch[i] == (char)(val-28) || ch[i] == (char)(val-24) || ch[i] == (char)(val-18) || ch[i] == (char)(val-12)){
				count++;
			}	
		}
		
		return count;
	}

	public static void main(String args[])throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();
		
		System.out.println(VowelCount(str));
	}
}
