/*WAP to print the Strong number between the given Range*/

import java.io.*;

class StrongNumber{
	
	static void StrongNumberPrint(int start, int end){
		
		for(int i= start ;i<= end; i++){
			
			if(i == 1){
				System.out.print(i + " ");
				continue;
			}

			int temp = i;
			int sum = 0;
			while(temp != 0){
				int mult = 1;

				for(int j = 2;j<=temp%10;j++){
					mult = mult * j;	
				}

				sum = sum+mult;

				temp = temp/10;
			}

			if(sum == i){
				
				System.out.print( i + " ");
			}
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting Number");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter the Ending Number");
		int end = Integer.parseInt(br.readLine());

		StrongNumberPrint(start,end);
	}
}
