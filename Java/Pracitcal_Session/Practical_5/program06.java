/*WAP to print the digit count of the number entered by the user*/

import java.io.*;

class DigitCount{
	
	static void DigitCountNumber(int Num){
		
		int count = 0;
		int temp = Num;

		while(temp != 0){
			count ++;
			temp = temp/10;
		}

		System.out.println("The Count of Digit in " + Num + " is: " + count);
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Count of times you will enter the number");
		int times = Integer.parseInt(br.readLine());
		while(times != 0){
			System.out.println("Enter the number to check the Digit count");
			int Num = Integer.parseInt(br.readLine());
			DigitCountNumber(Num);
			times--;
		}	
	}
}
