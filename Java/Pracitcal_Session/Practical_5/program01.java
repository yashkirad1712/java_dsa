/*WAP to find the number divisible by 5 from 1 to 50 & the number is even also print the count of even number*/


import java.io.*;

class Calculator{
	
	static int division(int lower ,int upper){
		
		System.out.println("The number are :");
		int count = 0;
		for(int i=lower ;i<=upper;i++){
			
			if(i%5 == 0 && i%2 == 0){
				count++;
				System.out.print(i + " ");
			}
		}
		System.out.println();
		return count;
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Lower limit");
		int lower = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Upper limit");
		int upper = Integer.parseInt(br.readLine());

		System.out.println("The Count of the numbers divisible by 5 is: " + division(lower,upper));
		
	}	
	
}
