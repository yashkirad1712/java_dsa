/*WAP to print the Armstrong number between the given Range*/

import java.io.*;

class ArmStrong{
	
	static void Armstrong(int start, int end){
		for(int i= start ;i<= end ;i++){
			
			int digit = (int)Math.log10(i) + 1;
				
			int sum =0;
			
			int temp = i;

			while(temp != 0){
				int val = temp%10;

				for(int j = 1; j < digit; j++){
					val = val*(temp%10);
				}
		
				sum = sum + val;
				temp = temp/10;
			}
		
			if(sum == i){
				System.out.println(i + " ");
			}
		}		

	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Staring Number");
		int start = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Ending Number");
		int end = Integer.parseInt(br.readLine());

		Armstrong(start,end);
	}
}
