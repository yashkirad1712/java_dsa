/*WAP to find the number which are Composite*/


import java.io.*;

class Composite{
	
	static void Print(int start ,int end){
		
		System.out.println("The Composite number are :");
		
		for(int i=start ;i<=end;i++){
			int count = 0;
			int j = 1;
			while(j!= i+1){
				
				if(i%j == 0){
					count ++;
				}
				j++;
			}

			if(count > 2){
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting Number");
		int lower = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Ending Number");
		int upper = Integer.parseInt(br.readLine());

		Print(lower,upper);
		
	}	
	
}
