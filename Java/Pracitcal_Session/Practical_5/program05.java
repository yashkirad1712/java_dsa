/*WAP to print the perfect number between a given range by the user*/

import java.io.*;

class PerfectNumber{
	
	static void PerfectNo(int start,int end){
		
		for(int i= start ;i<= end ;i++){

			int count = 0;

			for(int j = 1 ;j < i;j++){
				if( i == j){
					continue;
				}

				if(i%j == 0){
					count = count + j;
				}	
			}

			if(count == i){
				System.out.print(i + " ");
			}
		}
		
	 	System.out.println();	
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the Starting Number");
		int start = Integer.parseInt(br.readLine());	
		
		System.out.println("Enter the Ending Number");
		int end = Integer.parseInt(br.readLine());	

		PerfectNo(start,end);
	}
}
