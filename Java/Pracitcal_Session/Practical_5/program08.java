/*WAP to print the PallinDrome Number Between the given range by the user*/

import java.io.*;

class Pallindrome{
	
	static void PallindromNumber(int start, int end){
		
		for(int i=start ;i<= end;i++){
			int temp = i;
			int num = 0;

			while(temp != 0){
				num = num*10 + temp%10;
				temp = temp/10;
			}

			if(i == num ){
				System.out.print(i + " ");
			}
		}

		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting Number");
		int start = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Ending Number");
		int end = Integer.parseInt(br.readLine());

		PallindromNumber(start,end);
	
	}
}
