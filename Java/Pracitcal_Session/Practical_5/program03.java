/*WAP to Print the perfect Square number between A range*/


import java.io.*;

class PerfectSquare{
	
	static void Print(int start ,int end){
		
		System.out.println("The Perfect Square number are :");
		
		for(int i=start ;i*i<=end;i++){
			System.out.print(i*i + " ");
			
		}

		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting Number");
		int lower = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Ending Number");
		int upper = Integer.parseInt(br.readLine());

		Print(lower,upper);
		
	}	
	
}
