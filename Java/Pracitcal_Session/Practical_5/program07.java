/*WAP to print the number in reverse from the rang given by the user*/


import java.io.*;

class ReverseNumber{
	
	static void Reverse(int start, int end){
		
		for(int i= start;i<= end;i++){
			int temp = i;
			int num = 0;
			while(temp != 0){
				num = num*10 + temp%10;
				temp = temp/10;
			}

			System.out.println("The reverse of " + i + " is: " + num);
		}
	}

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting Number");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Enter the Ending Number");
		int end = Integer.parseInt(br.readLine());

		Reverse(start,end);
	}
}
