/* 10
 *  I H
 *  7 6 5
 *  D C B A*/

class Pattern{
	
	public static void main(String[] args){
		
		int row = 4;
		int num = 0;

		for(int n =1 ;n<=row ;n++){
			num = num + n;
		}
		
		char ch = (char)(64 + num);

		for(int i=1 ;i<= row ;i++){
			
			for(int j=1 ;j<= i ;j++){
				if(i %2 == 0){
					System.out.print(ch + " ");
				}else{
					System.out.print(num + " ");
				}

				num--;
				ch --;
			}
			System.out.println();
		}
	}
}
