/*
C2W10
C2W9 C2W8
C2W7 C2W6 C2W5
C2W4 C2W3 C2W2 C2W1 */

class Pattern{
	
	public static void main(String[] args){
		
		int row =4;
		
		int num = 0;
		for(int j=1 ;j<=row;j++){
			num = num + j;
		}

		for(int i=1 ;i<=row;i++){
			
			for(int k =1 ;k<=i ;k++){
				System.out.print("C2W" + num-- + " ");
			}
			
			System.out.println();	
		}
	}
}
