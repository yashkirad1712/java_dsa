/* 1
 * 3 4
 * 6 7 8
 * 10 11 12 13
 * 15 16 17 18 19*/

class Pattern{
	
	public static void main(String[] args){
		
		int row = 5;
		int num = 1;
		for(int i=1 ;i<= row ;i++){
			
			for(int j=1 ;j<=i ;j++){
				
				if(j == i){
					System.out.print(num + " ");
					num += 2;
				}else{
					System.out.print(num + " ");
					num++;
				}	
			}
			System.out.println();
		}
	}
}
