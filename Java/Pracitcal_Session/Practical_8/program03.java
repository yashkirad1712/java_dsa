/* 10
 * 10 9 
 * 9 8 7
 * 7 6 5 4*/


class Pattern{
	
	public static void main(String[] args){
		
		int row = 4,temp = 0;
		int num = 10;

		for(int i=1 ;i<= row ;i++){
			
			for(int j =1 ;j<=i ;j++){
				
				System.out.print(num + " ");
				temp = num;
				num--;
			}
			num = temp;
			System.out.println();
		}	

	}
}
