/*WAP to calculate the number of digits*/

class Digit{
	
	public static void main(String[] args){
		
		long n = 987654321;
		int count = 0;

		while(n != 0){
			count ++;
			n = n/10;
		}

		System.out.println(count);
	}
}
