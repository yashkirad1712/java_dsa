/*WAP to calculate the number of odd  digits*/

class OddCount{
	
	public static void main(String[] args){
		
		long n = 987654321;
		
		int oddCount = 0;

		while(n != 0){
			
			if((n%10) % 2 == 1)
				oddCount ++;

			n = n/10;
		}

		System.out.println(oddCount);
	}
}
