/*WAP to print the number if it is even in reverse or in=f odd then by the diff of 2 in reverse*/

import java.io.*;

class Reverse{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");
		int num = Integer.parseInt(br.readLine());

		if(num%2 == 0){
			while(num != 0){
				System.out.println(num);
				num--;
			}	
		}else{
			while(num > 0){
				System.out.println(num);
				num = num - 2;
			}	
		}
	}
}
