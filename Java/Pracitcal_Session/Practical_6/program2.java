/*WAP to calculate the Factorial of the Given number*/

class Factorial{
	
	public static void main(String[] args){
	
		int n = 6;
		int fact = 1;

		while(n != 1){
			fact = fact*n;
			n--;	
		}

		System.out.println(fact);
	}
}
