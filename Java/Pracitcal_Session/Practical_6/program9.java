/*WAP to reverse the number*/

class ReverseNum{
	
	public static void main(String[] args){
		
		long num = 987654;

		while(num != 0){
			
			System.out.print(num%10);
			num /=10;
		}

		System.out.println();
	}
}
