/*WAP to print the sum of all even and multiplication of odd number*/

class Sum{
	
	public static void main(String[] args){
		
		int n = 1;
		int sum =0,mult=1;

		while(n != 11){
			
			if(n%2 == 0){
				sum = sum + n;
			}else{
				mult = mult* n;
			}
			
			n++;
		}

		System.out.println("sum of even numbers are : " + sum);
		System.out.println("Multiplication of odd numbers are : " + mult);
	}
}
