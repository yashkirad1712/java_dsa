/*WAP to print the Days remaining*/

class Days{
	
	public static void main(String[] args){
		
		int day = 7;

		while(day != -1){
			
			if(day >=2){
				System.out.println(day + " Days is Remaining");
			}else if(day == 0){
				System.out.println("Time is Over");
			}else{
				System.out.println(day + " Day is Remaining");
			}

			day--;
		}
	}
}
