/*Stroing Number : A number is said to be strong if the sum of the factorial of the digits of the number is equal to the number*/

class Yash{
	public static void main(String args[]){
		int n=625;
		int sum =0;
		int temp = n;
		while(n!=0){
			int mult = 1;
			for(int i=1 ;i<=n%10;i++){
				mult = mult * i;
			}
			sum = sum + mult;
			n=n/10;
		}
		if(sum == temp)
			System.out.println("The number is Strong number");
		else
			System.out.println("The number is Not a Strong number");
	}
}
