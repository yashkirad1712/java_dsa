/*Automorphic Number: A number is said to be Automorphic if 
 	conditions:
	1.Calculate the square of the given number
	2.then count the digits of the given number and check them with same digits of the square number
	3. the number must be equal to the number's sqaure from last digit last digits
	example: 
	input : 5
		square :25
		given number has 1 digit
		check only one digit from last of the squared number
		i.e 5 == 5
		So automorphic number is 5
*/

class Yash{
	public static void main(String args[]){
		int n = 76;
		int square = n*n;
		int flag = 0;
		while(n!=0){
			if(n%10 != square%10){
				flag = 1;
				System.out.println("Not a Automorphic number");
			}
			n = n/10;
			square =square/10;
		}
		
		if(flag == 0)
			System.out.println("The numbe is Automorphic");
		
	}
}
