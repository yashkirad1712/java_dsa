/*Perfect Number:A number is said to be perfect if the sum of the factors of the number is equal to the number itself*/

class Yash{
	public static void main(String args[]){
		int n=14;
		int sum =0;
		for(int i=1;i<=n;i++){
			if(n%i == 0){
				sum = sum+i;
			}
		}	
		if(sum - n == n){
			System.out.println("Perfect Number");
		}else{
			System.out.println("Not a perfect number");
		}
	}
}
