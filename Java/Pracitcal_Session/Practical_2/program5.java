/*Fibonacci Series : a series where the the next number is the sum of the preceding two numbers
  ex: 0 1 1 2 3 5 8 .... */

class Yash{
	public static void main(String[] args){
		int prev = 0;
		int curr = ++prev;
		for(int i=1;i<=7;i++){
			System.out.println(prev);
			int next = prev + curr;
			prev = curr;
			curr = next;

		}
	}
}
