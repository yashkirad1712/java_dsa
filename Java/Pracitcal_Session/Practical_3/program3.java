/*5 4 3 2 1
  8 6 4 2
  9 6 3
  8 4
  5*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of Rows");
		int Row = Integer.parseInt(br.readLine());
		
		for(int i=1 ;i<= Row;i++){
			int var = (Row-i)+1;
			for(int j=1 ;j<=(Row-i)+1;j++){
				System.out.print(var*i + " ");
				var--;
			}
			System.out.println();
		}
	}
}
