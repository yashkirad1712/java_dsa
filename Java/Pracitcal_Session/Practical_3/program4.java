/*Wap to print the all even numbers in reverse order and all odd numbers in standard way
 * input= 2 to 9
 * o/p = 8 6 4 2
 *       3 5 7 9*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start Range");
		int Start = Integer.parseInt(br.readLine());
		
		System.out.println("Enter End Range");
		int End = Integer.parseInt(br.readLine());
		
		if(End < Start){
			int temp = Start;
			Start = End;
			End = temp;
		}

		for(int j=End;j>=Start;j--){
			if(j%2 == 0){
				System.out.print(j +" ");
			}
		}	
		
		System.out.println();

		for(int i=Start ;i<=End;i++){
			if(i%2 != 0){
				System.out.print(i + " ");
			}
		}
		System.out.println(" ");
	}
}		
