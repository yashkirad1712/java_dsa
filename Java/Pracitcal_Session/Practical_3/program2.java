/*# = = =
  = # = =
  = = # =
  = = = #*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Number of Rows");
		int Row = Integer.parseInt(br.readLine());

		for (int i=1;i<=Row;i++){
			for(int j=1;j<=Row;j++){
				if(i == j){
					System.out.print("# ");
				}else{
					System.out.print("= ");
				}
			}
			System.out.println();
		}
	}
}
