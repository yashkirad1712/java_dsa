/*WAp to print the difference of the two alphabets if they dont are equal and if they are equal tehn print the equal alphabet*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the First Alphabet");
		char ch1 = (char)br.read();
	       	br.skip(1);	
		System.out.println("Enter the Second Alphabet");
		char ch2 = (char)br.read();
	       	br.skip(1);
		
		if(ch1 < ch2){
			char temp = ch1;
			ch1 = ch2;
			ch2 = temp;
		}

		if(ch1 == ch2){
			System.out.println(ch1 +" is the given Alphabet which is equal");
		}else{
			int diff = ch1 - ch2;
			System.out.println("The difference between " + ch1 + " & " + ch2 +" is :" + diff);
		}	
	}
}
