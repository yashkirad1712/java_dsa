/*WAP to print the take start and end from the user and print the prime numbers in that range*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the starting range");		
		int Start = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the starting range");		
		int End = Integer.parseInt(br.readLine());
		
		System.out.print("The prime numbers: ");
		
		for(int i = Start;i<=End;i++){
			int count =0;
			for(int j =1;j*j <= i;j++){
				if(j%i == 0){
					count = count + 2;
				}
			}

			if(count == 2){
				System.out.print(i);
			}	
		}
	}
}
