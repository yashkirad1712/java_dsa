/*0
  1 1
  2 3 5
  8 12 21 34*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		System.out.println("Enter the number of rows");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int Row = Integer.parseInt(br.readLine());
		int prev = 0;
		int curr = 1;
		for(int i=1;i<=Row;i++){
			for(int j=1;j<=i;j++){
				
				System.out.print(prev + " ");
				int next = prev + curr;
				prev = curr;
				curr = next;
			}
			System.out.println();
		}
	}
}
