/*D4 C3 B2 A1
  A1 B2 C3 D4
  D4 C3 B2 A1
  A1 B2 C3 D4*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int Row = Integer.parseInt(br.readLine());
		
		char ch = (char)(65+Row);
	        int num = Row + 1;
			
		for(int i=1;i<=Row;i++){
			for(int j=1;j<=Row;j++){
				if(i%2 != 0){
					--ch;
					--num;
					System.out.print(ch);
					System.out.print(num);
					System.out.print(" ");
				}else{
					System.out.print(ch++);
					System.out.print(num++);
					System.out.print(" ");
				}	
			}
			System.out.println();
		}
	}
}
