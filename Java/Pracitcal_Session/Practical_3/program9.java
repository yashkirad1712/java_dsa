/*WAP that prints the sum of the factorial of the digit of the given number and print it*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number :");
		int Num = Integer.parseInt(br.readLine());
		int sum = 0;
		while(Num != 0){
			int mult = 1;
			for(int i=2;i<=Num%10;i++){
				 mult = mult*i;
			}
			sum = sum + mult;
			Num = Num/10;
		}

		System.out.println("Sum of the : " + sum);


	}
}
