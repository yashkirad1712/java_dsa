/*O
  14 13
  L  K  J
  9  8  7  6
  E  D  C  B  A*/

import java.io.*;

class Yash{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number of Rows");
		int Row = Integer.parseInt(br.readLine());
		
		int sum = 0;
		for(int k=1;k<=Row;k++){
			sum = sum+k;
		}
		
		char ch = (char)(64 + sum);
		
		for(int i=1 ;i<= Row;i++){
			for(int j=1; j<=i;j++){
				if(Row % 2 == 1){
					if(i%2 != 0){
						System.out.print(ch + " ");
					}else{
						System.out.print(sum + " ");
					}
				}else{
					if(i%2 == 0){
						System.out.print(ch + " ");
					}else{	
						System.out.print(sum + " ");
					}
				}
				ch--;
				sum--;
			}
			System.out.println();
		}
	}
}
