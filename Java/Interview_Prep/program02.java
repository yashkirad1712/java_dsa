/*Using 2 this in same constructor*/


class Demo{
	
	Demo(int x){
		
		System.out.println(x);
	}

	Demo(int y, int x){
		System.out.println(x+y);
	}

	Demo(){
		this(5);
		this(5,6);
	}
}

class Client{
	
	public static void main(String[] args){
	
		Demo obj = new Demo();

	}
}
/*program02.java:17: error: call to this must be first statement in constructor
                this(5,6);
                    ^
1 error*/
