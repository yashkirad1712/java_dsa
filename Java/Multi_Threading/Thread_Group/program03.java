/*Getting the brief explanation of what basssically all the methods do internally*/


class Mythread extends Thread{

	Mythread(String str){
		super(str);
	}

	Mythread(ThreadGroup tg , String str){
		super(tg,str);
	}

	Mythread(){
		super();
	}

	public void run(){
		System.out.println(Thread.currentThread());
		System.out.println(Thread.currentThread().getName());
		
		
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		Mythread obj = new Mythread();
		obj.start();
		try{	
			obj.join();
		}catch(InterruptedException ie){
			System.out.println("There is an Interruption Occured in the thread");
		}

		ThreadGroup MyGroup = new ThreadGroup("Practice");
			
		Mythread obj1 = new Mythread(MyGroup,"DemoGroup");
  		obj1.start();

	}
}
