/*Real Time Example of Thread Group:
 * Creating and Hotel Scenario:
 * Parent ThreadGroup: Hotel
 * Child ThreadGroup: Menu Card*/

import java.io.*;

class Database extends Thread{
	
	Database(String str){
		super(str);
	}

	Database(ThreadGroup tg, String str){
		super(tg,str);
	}

	Database(){
		super();
	}	

	public void run(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Hotel Name To Present The Menu Card:");
		String name = " ";
		try{
			name = br.readLine();
		}catch(IOException ie){
			System.out.println("Input Connection Gone");
		}

		System.out.println("Name of the Hotel is: " +name);
		System.out.println("The Thread Assigned to you is: " + Thread.currentThread());
	}
}

class MenuDatabase extends Thread{
	
	MenuDatabase(String str){
		super(str);
	}

	MenuDatabase(ThreadGroup tg, String str){
		super(tg,str);
	}

	MenuDatabase(){
		super();
	}	

	public void run(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Dish Name To Present That Item From Card:");
		String name = " ";
		try{
			name = br.readLine();
		}catch(IOException ie){
			System.out.println("Input Connection Gone");
		}

		System.out.println("Name of the Dish Ordered is: " +name);
		System.out.println("The Thread Assigned to you is: " + Thread.currentThread());
	}
}

class Client{
	
	public static void main(String[] args)throws InterruptedException{
		
		ThreadGroup India = new ThreadGroup("Restaraunts");
		ThreadGroup Maharashtra = new ThreadGroup(India,"Maha-Rest");

		Database user1 = new Database(India,"Customer1");
		user1.start();
		user1.join();
		System.out.println("----------------------------------");
		MenuDatabase user2 = new MenuDatabase(Maharashtra,"Menucard-1");
		user2.start();
		user2.join();
		System.out.println("----------------------------------");
		System.out.println("----------------------------------");
		
		Database user3 = new Database(India,"Customer2");
		user3.start();
		user3.join();
		System.out.println("----------------------------------");
		MenuDatabase user4 = new MenuDatabase(Maharashtra,"Menucard-2");
		user4.start();
		user4.join();
		System.out.println("----------------------------------");
		System.out.println("----------------------------------");
		
		Database user5 = new Database(India,"Customer3");
		user5.start();
		user5.join();
		System.out.println("----------------------------------");
		MenuDatabase user6 = new MenuDatabase(Maharashtra,"Menucard-3");
		user6.start();
		user6.join();
		System.out.println("----------------------------------");
		System.out.println("----------------------------------");

	}
}



