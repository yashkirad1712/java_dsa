/*THread Group Real Time Example*/

import java.io.*;

class InstaDataBase extends Thread{
	
	InstaDataBase(ThreadGroup tg, String str){
		
		super(tg,str);
	}

	InstaDataBase(String str){
		
		super(str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
		PersonalInfo();
	}

	void PersonalInfo(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter your Instagram ID");
		String name = " ";
		try{
			name = br.readLine();
		}catch(IOException ie){
			System.out.println("Input Connection Gone");
		}

		System.out.println("Instagram Account Holder:" + name);
	
	}
}	

class ThreadsDataBase extends Thread{

	ThreadsDataBase(ThreadGroup tg, String str){
		
		super(tg,str);
	}

	ThreadsDataBase(String str){
		
		super(str);
	}

	public void run(){

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println(Thread.currentThread());

		System.out.println("Enter your Thread ID");
		String name = " ";
		try{
			name = br.readLine();
		}catch(IOException ie){
			System.out.println("Input Connection Gone");
		}

		System.out.println("Instagram Thread Account Holder:" + name);
	}	

}


class Client{
	
	public static void main(String[] args)throws InterruptedException{
		
		ThreadGroup Users = new ThreadGroup("Instagram");
		ThreadGroup NewUsers = new ThreadGroup(Users,"Threads-Insta");

		InstaDataBase User1 = new InstaDataBase(Users,"Yash");
		InstaDataBase User2 = new InstaDataBase(Users,"Mark");
		InstaDataBase User3 = new InstaDataBase(Users,"Elon");
		
		ThreadsDataBase User4 = new ThreadsDataBase(Users,"Yash-Threads");
		ThreadsDataBase User5 = new ThreadsDataBase(Users,"Mark-Threads");
		ThreadsDataBase User6 = new ThreadsDataBase(Users,"Elon-Threads");

		User1.start();
		User1.join();
		System.out.println("----------------------");
		User4.start();
		User4.join();
		System.out.println("----------------------");
		System.out.println("----------------------");

		User2.start();
		User2.join();
		System.out.println("----------------------");
		User5.start();
		User5.join();
		System.out.println("----------------------");
		System.out.println("----------------------");

		User3.start();
		User3.join();
		System.out.println("----------------------");
		User6.start();
		User6.join();
	}
}













