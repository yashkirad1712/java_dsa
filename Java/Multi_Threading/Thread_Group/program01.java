/*Creating thread Group*/

class MyThread extends Thread{
	
	MyThread(String str){
		
		super(str);
	}

	MyThread(ThreadGroup tg ,String str){
		
		super(tg,str);
	}

	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		ThreadGroup pThreadGP = new ThreadGroup("Core2web");

		MyThread obj1 = new MyThread(pThreadGP,"BootCamp");
		MyThread obj2 = new MyThread(pThreadGP,"Java,DSA");
		MyThread obj3 = new MyThread(pThreadGP,"OS");
		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"Incubator");
		MyThread obj4 = new MyThread(pThreadGP,"Flutter");
		MyThread obj5 = new MyThread(pThreadGP,"SpringBOOT");
		MyThread obj6 = new MyThread(pThreadGP,"ReactJS");
		obj4.start();
		obj5.start();
		obj6.start();
	}
}
