/*IllegalThreadStateException : Runtime*/

/*We Cannot reInitialize the same thread i.e if it is already running we cannot take it to thta state again or in any other state by our own it is not allowed*/


class MyThread implements Runnable{
	
	public void run(){
		Thread T = Thread.currentThread();
		System.out.println("In Run: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());

	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		MyThread obj = new MyThread();
		Thread T = new Thread(obj);
		
		System.out.println("In Main: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());
		T.start();

		T.start();// Trying to change the state of the already running state again into running state by our own
	}
}

/*In Main: main Has Priority : 5
In Run: Thread-0 Has Priority : 5
Exception in thread "main" java.lang.IllegalThreadStateException
        at java.lang.Thread.start(Thread.java:710)
        at ThreadDemo.main(program05.java:23)*/
