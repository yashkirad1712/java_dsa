/*IllegalArgumentException : RunTime;
 * 
 *
 * Generation by Assigning the priority of the thread lesser then 1 or greater then 10 ;
 * Main has Default of 5 Priority
 * max: 10
 * min : 1*/


class MyThread implements Runnable{
	
	public void run(){
		Thread T = Thread.currentThread();
		T.setPriority(-1);
		System.out.println("In Run: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());

	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		MyThread obj = new MyThread();
		Thread T = new Thread(obj);

		T.start();
		T.setPriority(10);
		System.out.println("In Main: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());
	}
}

/*In Main: main Has Priority : 10
Exception in thread "Thread-0" java.lang.IllegalArgumentException
        at java.lang.Thread.setPriority(Thread.java:1094)
        at MyThread.run(program04.java:11)
        at java.lang.Thread.run(Thread.java:750)*/
