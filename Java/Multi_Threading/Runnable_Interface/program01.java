/*Creating A thread USing Runnable Interface gives Extra Power to the Class as it allows the class to extend to another parent then thread as Runnable interface is their for thread Creation
 *
 * Runnable Interfcae
 *         |
 *         |
 *        \/
 * Thread Class*/

class MyThread implements Runnable{
	
	public void run(){
		System.out.println("In Run Method :" + Thread.currentThread().getName());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		MyThread obj = new MyThread();
		Thread T = new Thread(obj);  //Creating an Object of thread Class and calling the Parameterized Constructor of it for calling the thread Create Method i.e VmThread.Create
		
		T.start();
		System.out.println("In Main :" + Thread.currentThread().getName());
	}
}
