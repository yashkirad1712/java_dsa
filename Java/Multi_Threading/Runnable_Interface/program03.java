/*THread Priority: In Threads Thread Scheduler Schedules the thread as per their priority and Child Class get The priority same as the parent thread ;
 * Main has Default of 5 Priority
 * max: 10
 * min : 1*/


class MyThread implements Runnable{
	
	public void run(){
		Thread T = Thread.currentThread();
		T.setPriority(7);
		System.out.println("In Run: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());

	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		MyThread obj = new MyThread();
		Thread T = new Thread(obj);

		T.start();
		T.setPriority(10);
		System.out.println("In Main: " + Thread.currentThread().getName() + " Has Priority : " + T.getPriority());
	}
}
