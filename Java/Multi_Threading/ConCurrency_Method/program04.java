/*Showing the dependency of the join method is on only the objects thread which is passed to it and no other thread then that*/


class NewThread extends Thread{
	
	public void run(){
		
		for(int h=0 ;h<=10;h++){
			System.out.println("In Thread-1");   //this thread will work but Main is not dependent on this threads execution
		}
	}
}

class Mythread extends Thread{
	
	public void run(){
		
		NewThread obj = new NewThread();
		obj.start();

		for(int j=0 ;j<=10;j++){
			
			System.out.println("In Thread-0"); //Main thread will wait unti this is done
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args)throws InterruptedException{
		Mythread obj = new Mythread();
		obj.start();
		obj.join();
		System.out.println("IN Thread Main");
	}
}
