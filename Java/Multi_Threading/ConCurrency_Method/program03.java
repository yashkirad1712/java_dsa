/*Creatring A situation Called DeadLock*/


class Close extends Thread{
	
	static Thread wait = null;

	public void run(){
		try{
			wait.join();
		}catch(InterruptedException ie){
		
		}

		System.exit(0);
	}
}

class DeadLock extends RuntimeException{
	
	DeadLock(String msg){
		super(msg);
		Close.wait = Thread.currentThread();
		Close obj = new Close();
		obj.start();

	}
	
}

class Mythread extends Thread{
	
	static Thread nmMain = null;
	
	public void run(){
		
		MyException obj1 = new MyException();

		try{
			obj1.join(3000);
			obj1.start();
		}catch(InterruptedException ie){
			System.out.println("Interrupted Excpetion");
		}

		try{
			MyException.flag1 = 1;
			nmMain.join();
		}catch(InterruptedException ie){
			System.out.println("InterruptedException");
		}	
	}
}

class MyException extends Thread{

	static int flag1 = 0;
	static int flag2 = 0;

	static void BreakLock(){
	
		if(flag1 == 1 && flag2 == 1){
			throw new DeadLock("DEADLOCK OCCURRED :All Processes are Killed");
		}
	
	}
	
	public void run(){
		BreakLock();
		
	}	
}

class ThreadDemo{
	
	public static void main(String[] args)throws InterruptedException{
		
		Mythread.nmMain = Thread.currentThread();  //returns Main Thread Object;

		Mythread obj = new Mythread();
		obj.start();
		MyException obj1 = new MyException();
		obj1.start();

		obj1.join(3000);
		
		MyException.flag2 = 1;
		obj.join();
	}
}
