/*Yield Method : static method basically used for giving single time Quantum get by the higher proiorty method to another method which is either in its level or more higher priority*/


class Mythread extends Thread{
	
	public void run(){
		
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo{

	public static void main(String[] args){
		
		Mythread obj = new Mythread();
		obj.start();

		obj.yield();
		System.out.println(Thread.currentThread().getName());
	}
}
