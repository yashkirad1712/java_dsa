/*Creating threads with user defined name*/


class Mythread extends Thread{
	
	Mythread(){
		super();
	}

	Mythread(String str){
		super(str);
	}

	public void run(){
		
		System.out.println(getName());  //method from thread class of getting the name of the thread is inherited by Mythread class so it can be directly called
		
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		Mythread obj1 = new Mythread("Xyz");
		obj1.start();
		
		Mythread obj2 = new Mythread("Xyz");
		obj2.start();
		
		Mythread obj3 = new Mythread();
		obj3.start();
	}
}
