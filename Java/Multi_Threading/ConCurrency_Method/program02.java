/*Join : this concurrency method is used to stop the assigned thread until the thread for which it is assigned is not completed its working*/


class Mythread extends Thread{
	
	public void run(){
		
		System.out.println(Thread.currentThread());
		for(int i=0 ;i<=10;i++){
			System.out.println("In Run");
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		Mythread obj = new Mythread();
		obj.start();
		
		try{
			obj.join();  // internals : obj = Thread-0, join(Thread-0) i.e Main Will stop until Thread-0 executes completely
		}catch(InterruptedException ie){
			System.out.println("Interruption betwen the Threads");
		}

		for(int j=0;j<=10;j++){
			System.out.println("In Thread Main");
		}
	}
}
