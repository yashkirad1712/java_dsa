/*Concurrency Method*/
/*In Java there are Three Concurrency method which are used to basically Decide the dependency and execution of the different threads */


class Mythread extends Thread{
	
	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo {
	
	public static void main(String[] args){
		
		Mythread obj = new Mythread();
		obj.start();
		
		try{
			Thread.sleep(1000); //Stops the execution of this thread until the given time span
		}catch(InterruptedException ie){
			System.out.println("Threads are Interrupting each Other");
		}

		Thread.currentThread().setName("Core2Web");
		System.out.println(Thread.currentThread());
	}
}
