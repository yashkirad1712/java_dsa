/*In Multithreaeding the class which extends the thread class is not allowed to throw any Exception as the parent Thread class run method is not dding this and i.e why child class is not allowed for it*/

class MyThread extends Thread{
	
	public void run(){
		for(int i=0 ;i<=10;i++){
			System.out.println("In Run");

			try{
				Thread.sleep(1000);   //1 sec == 1000 ms
			}catch(InterruptedException ie){
				System.out.println("Interrupted exception appeared");
			}
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args)throws InterruptedException{
		
		MyThread obj = new MyThread(); //thread Created
		obj.start();

		for(int i=0 ;i<=10;i++){
			System.out.println("In Main");
			Thread.sleep(1000);
		}	

	}
}
