/*Overriding the start Method unable the thread to run in its respected Area*/

class MyThread extends Thread{
	
	public void run(){
		
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}

/*	public void start(){
		
		System.out.println("In start");
		run();
		System.out.println(Thread.currentThread().getName());	

	}
*/
	/*Here if we do this then the start method get overriden and then the new thread doesent get its allocation to the area where he needs to run*/
}

class ThreadDemo{
	
	public static void main(String[] args){
		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());

	}
}
