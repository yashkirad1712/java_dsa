/*For MultiThreading we have to compulsory override the run method for running the new thred on that method*/

class My_Thread extends Thread{
	
	//here our new thread will target and run
	public void run(){
		for(int i=0 ;i<= 4;i++){
			for(int j=0 ;j<= i ;j ++){
				System.out.println("Yash I Love You");
			}
		}
	}

}

class ThreadDemo{
	
	public static void main(String[] args){
		
		My_Thread obj = new My_Thread(); //creation of thread
		obj.start();   //thread will enter the respected area for getting execute its work i.e : run method
		
		for(int i = 0 ;i<=5 ;i++){
		
			System.out.println(i);
		}
	}
}
