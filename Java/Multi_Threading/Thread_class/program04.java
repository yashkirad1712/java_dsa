/*Creating two threads in Different Classes*/


class Demo1 extends Thread{
	
	public void run(){
		
		System.out.println("In Demo1 :" + Thread.currentThread().getName());

		Demo2 obj = new Demo2();  //creation of thread 2
		obj.start();
	}
}

class Demo2 extends Thread{
	
	public void run(){
		
		System.out.println("In Demo2 :" + Thread.currentThread().getName());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
	
		Demo1 t = new Demo1();
		t.start();

		System.out.println("In Main: " + Thread.currentThread().getName());
	}
}
