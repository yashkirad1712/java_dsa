/*1  2  9
  4 25  6
  49 8  81*/

class Yash{
	public static void main(String args[]){
		int n=1;

		for(int i=1;i<=9;i++){
			if(i%3 == 0){
				if(i%2 == 1){
					System.out.print(n*n + " ");
				}else{
					System.out.print(n + " ");
				}	
				System.out.println();
				n++;
				continue;
			}

			if(i%2 == 1)
				System.out.print(n*n + " ");
			else
				System.out.print(n + " ");

			n++;
		}
	}
}
