/*Throw clause : it is used to throw the exception before it actually occurs i.e. it terminates the code there only by notifying the user not to do this or there will be exception*/

import java.util.*;

class Demo{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		int x= sc.nextInt();

		try{	
			if(x == 0)
				throw new ArithmeticException("Divide / 0");
			
			System.out.println(10/x);
		}catch(ArithmeticException ae){
			String str = Thread.currentThread().getName();

			System.out.println("Exception in thread" + str );
			ae.printStackTrace();
		}

	}
}
