/*Null Pointer Exception : Run Time Exception*/

class Demo{
	
	void m1(){
		System.out.println("In M1");
	}

	void m2(){
		System.out.println("In M2");
	}

	public static void main(String[] args){
	
		Demo obj = new Demo();
		obj.m1();
		obj = null;
		obj.m2();
	}
}

/*In M1
Exception in thread "main" java.lang.NullPointerException
        at Demo.main(program04.java:18)*/
