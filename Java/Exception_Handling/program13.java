/*HAndling Exception in Array Reversing of Input Output*/

import java.io.*;

class Demo{
	
	static void ReverseArray(int[] arr){
		
		//int[] rev = new int[arr.length];
		int temp = 0;
		int k = 0;
		for(int j= (arr.length-1); j>=(arr.length/2);j--){
			temp = arr[k];
			arr[k++] = arr[j];
			arr[j] = temp;
		}

		System.out.println("Reversed Array is:");
		for(int x : arr){
			System.out.println(x);
		}
	}

	public static void main(String[] args){
			
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of digits");
		int num = 0;
		try{
			num = Integer.parseInt(br.readLine());
		}catch(IOException ie){
			System.out.println("Input Connection Gone");
		}

		int[] arr = new int[num];

		System.out.println("Enter the values in the array");

		for(int i=0 ;i<arr.length; i++){
			try{
				arr[i] = Integer.parseInt(br.readLine());
			}catch(IOException ie){
				System.out.println("Input Connection Gone");
			}	
		}

		System.out.println("The array so Fromed is:");
		
		for(int x : arr){
			System.out.println(x);
		}

		ReverseArray(arr);
	
	}
}
