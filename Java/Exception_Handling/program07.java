/*Try catch block:
 *
 * try : contains the risky code
 * catch : contains the handler code
 */

class Demo{
	
	public static void main(String[] args){
		
		System.out.println("In Main");

		//genertaing the exception here i.e. Arithmetic exception(Run Time Exception) and it is catched 
		try{
			System.out.println(10/0);
		}catch(ArithmeticException ae){
			System.out.println("Dividing with illegal Denominator");
		}

		System.out.println("End Main");
		
	}
}
