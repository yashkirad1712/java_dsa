/*IO exception:Compile Time Exception : must be caught or declared to be thrown if present : or there is a abnormal termination of the code*/

import java.io.*;

class Demo{
	
	public static void main(String[] args){//throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		//type 1;
		String str = br.readLine();
		//exception will be thrown here as it will check the connection of the pipe and if it is not checked then it will throw an exception reagarding to it
		
		//type 2:
		br.close();
	}
}

/*program06.java:12: error: unreported exception IOException; must be caught or declared to be thrown
                String str = br.readLine();
                                        ^
program06.java:16: error: unreported exception IOException; must be caught or declared to be thrown
                br.close();
                        ^
2 errors*/
