/*Nested Try catch Block*/

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number");
		int num = 0;

		try{
			num = Integer.parseInt(br.readLine());
		}catch(NumberFormatException ie){
			System.out.println("Input is wrong");
			try{
				num = Integer.parseInt(br.readLine());
			}catch(NumberFormatException nme){
				System.out.println("Enter Integer only");
				num = Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Integer :" + num);
	}	

}
