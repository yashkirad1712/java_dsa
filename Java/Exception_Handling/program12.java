/*User Defined EXception creationg my Exception calss*/

import java.util.*;

class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){
		
		super(msg);
	}
}

class DataUnderFlowException extends RuntimeException{
	
	DataUnderFlowException(String msg){
		
		super(msg);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number");
		int num = sc.nextInt();

		if(num > 10){
			throw new DataOverFlowException("User choosing high Values");
		}else if(num < 0){
			throw new DataUnderFlowException("User choosing Low Values");
		}
	}
}
