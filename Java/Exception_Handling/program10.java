/*Finally Block: it is basically uSed for the connectivity purpose and when there is an abnormal termination of the code the flow of termination goes within it and then terminates i.e why we can use it for closing the connectivity of the databases before termination*/

class Demo{
	
	void m1(){
		System.out.println("In M1");
	}

	public static void main(String[] args){
		System.out.println("Start Main");
		Demo obj = new Demo();
		obj.m1();
		try{
			obj = null;
		}catch(ArithmeticException ae){
			System.out.println("EXception handled");
		}finally{
			System.out.println("Closing the connection successfully");
		}	

		System.out.println("End Main");  //this part is going to terminate
	}
	
	
}
