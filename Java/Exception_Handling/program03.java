/*DEfault Exception HAndler*/

//When we dont write an EXception for any runtime/unchecked exception it is passes to the Feature of JVM i.e Default Exception Handler


class Demo{
	
	void m1(){
		
		System.out.println("In M1");
		System.out.println(10/0);  //ArithemeticException not caught here
		m2();
	}

	void m2(){
		
		System.out.println("In M1");
	}

	public static void main(String[] args){
		
		System.out.println("Start Main");
		Demo obj = new Demo();
		obj.m1();
		System.out.println("End Main");
	}

}
/*Start Main
In M1
Exception in thread "main" java.lang.ArithmeticException: / by zero
        at Demo.m1(program03.java:11)
        at Demo.main(program03.java:24)*/


//when DEH Comes in picture the flow of the code is disrupted i.e Abnormal Termination Occur
