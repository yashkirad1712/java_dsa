/*Mine Example of User Defined Exception*/

/*Ticket Checking System*/

import java.io.*;

class InvalidUser extends RuntimeException{
	
	InvalidUser(String msg){
		super(msg);
	}
}

class TicketCheck{
	
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Crowd Count");
		int headCount = Integer.parseInt(br.readLine());

		int[] ticketID = new int[headCount];

		for(int i=0 ; i< headCount ; i++){
			
			ticketID[i] = Integer.parseInt(br.readLine());
		}

		for(int x : ticketID){
				if(x > 1000 || x < 100)
				throw new InvalidUser("--- Viewer Not Registered ---");

			System.out.println("Viewer ID :" + x);
		}
	}
}
