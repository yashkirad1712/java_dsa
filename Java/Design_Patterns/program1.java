/*Design Patterns*/

/*Singleton Pattern*/

class Singleton{
	
	static Singleton singleton = new Singleton();
	
	private Singleton(){
		System.out.println("In Constructor");
	}

	static Singleton getData(){
		return singleton;
	}
}

class Client{
	
	public static void main(String[] args){
		
		Singleton obj1 = Singleton.getData();
		System.out.println(obj1);

		Singleton obj2 = Singleton.getData();
		System.out.println(obj2);

		Singleton obj3 = Singleton.getData();
		System.out.println(obj3);
	}
}
