/*If else*/

class Yash{
	public static void main(String[] args){
		if(true)
			System.out.println("Inside if 1");
			System.out.println("Inside if 2");
		else
			System.out.println("Inside else");	
	}


}
/*Error will occur becuse the first if has no braces so it will end after the first line executes so the else block is without the if block so it will give the compile time error*/
