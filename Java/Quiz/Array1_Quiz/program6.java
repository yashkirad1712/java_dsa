/*Array Syntax without dimensions*/


class Yash{
	
	public static void main(String args[]){
		
		int [] arr =new int[]{10,20,30,40,50};

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
		
		int [] arr1 ={1,2,3,4,0};

		for(int i=0;i<arr1.length;i++)
			System.out.println(arr1[i]);
	}
}
