/*An array cannot be initialized as shown because the compiler will raise error.This way the array can be initialized but both declarations and assignments should be done on the same line*/

class Yash{
	public static void main(String args[]){
		
		int [] arr;
		arr = {1,2,3,4};

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i] + " ");
		}
	}
}
