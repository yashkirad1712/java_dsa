/*Quiz 1 : 3*/

class Yash{

	public static void main(String args[]){
		
		String str1 = "This is a String";
		String str2 = "a String";
		String str3 = "This is " + str2;
		

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str3));
		

		if(System.identityHashCode(str1) == System.identityHashCode(str3)){
			System.out.println("Equal");
		}else{
			System.out.println("Not Equal");
		}
	}
}
