/*int array in a string*/


class Yash{
	
	public static void main(String args[]){
		
		
		char[] carr = {'a','c','d'};
		String str1 = new String(carr);
		System.out.println(str1);

		int[] arr = {9,7,8,6};
		String str = new String(arr);

		System.out.println(str);
		/* error: no suitable constructor found for String(int[])
                String str = new String(arr);*/
	}
}
