/*Creating an Annonymous Inner Class*/


class Demo{
	
	void marry(){
		
		System.out.println("Disha Patani");
	}
}

class CLient{

	public static void main(String[] args){
		
		Demo obj = new Demo(){
			void marry(){
				
				System.out.println("Kriti Sanon");
			}
		
		};  //creation of Annoynmous Inner class
		//here the name of the Annonymous Inner class will be "Client$1.class"
		
		
		obj.marry();
		//here when we call the marry function it will call the Annonymous class marry method as it will extend the demo class and overide it i.e overriding is done here but only for one use.`
	}

}
