// Java Program to Demonstrate Anonymous inner class

// Interface
interface Age {
	int x = 21;
	void getAge();
}

// Main class
class AnonymousDemo {

	// Main driver method
	public static void main(String[] args)
	{

		/*Here we created the object of age interface which instatntiates the anonymous inner class associated with it which executes its method get age when we call it with the help of the obj1*/
		@Override Age obj1 = new Age() {
		
			public void getAge()
			{
				// printing age
				System.out.print("Age is " + x);
			}
		};
	
		obj1.getAge();
	}
}

