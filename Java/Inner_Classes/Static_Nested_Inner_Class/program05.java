/*Example*/

class Parent{
	
	Parent(){
		System.out.println("In Parent Constructor");
	}

	int x = 10;
	static int y = 20;

	void m1(){
		System.out.println("In PArent M1");
	}

	static void m2(){
		
		System.out.println("In PArent m2");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In child Constructor");
	}

	int a = 7;
	static int b= 20;

	void m1(){
		System.out.println("In child m1");
	}

	static void m3(){
		
		System.out.println("In Child m3");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Parent();
		obj1.m1();
		obj1.m2();

		Child obj2 = new Child();
		obj2.m1();
		obj2.m3();

		Parent obj3 = new Child();
		obj3.m1();
		obj3.m2();

	}
}
