/*giving class object as a parameter to the object and accessiong the methods*/


class Parent{
	
	int x= 10;
	
	void m1(){
		
		System.out.println("In Parent m1");
	}
}

class Child extends Parent{
	
	int a = 20;

	void m1(){
		
		System.out.println("In child M1");
	}
}

class Demo{
	
	Demo(Parent p){
		
		System.out.println("In Constructor : Parent");
		p.m1();
	}

	Demo(Child c){
		
		System.out.println("In Constructor : Child");
		c.m1();
	}

	public static void main(String[] args){
		
		Demo obj1 = new Demo(new Parent());
		Demo obj2 = new Demo(new Child());
	}
}
