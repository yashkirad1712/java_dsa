/*Logical Example*/

class Demo{
	
	Demo(){
	
		System.out.println("In Constructor Demo");
	}
}

class DemoChild extends Demo{
	
	DemoChild(){
		
		System.out.println("In Constructor DemoChild");
	}
}

class Parent{
	
	Parent(){
		
		System.out.println("In Constructor Parent");
	}

	Demo m1(){
		
		System.out.println("In m1 Parent");
		return new Demo();
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("In Constructor Child");
	}

	DemoChild m1(){
		
		System.out.println("In DemoChild");
		return new DemoChild();
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent P = new Child();
		P.m1();
	}
}
