/*Loosly Coupling of Static Inner class*/


class Outer{
	
	static class Inner{
		
		void m1(){
			System.out.println("In M1 Inner");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer.Inner();  // loosely coupled
		obj.m1();
	}
}
