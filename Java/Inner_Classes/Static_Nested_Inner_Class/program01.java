/*Static nested Inner Class*/

class Outer{
	
	void m1(){
		
		System.out.println("In M1 Outer");
	}

	static class Inner{
		
		void m2(){
			System.out.println("In M2 Inner");
		}
	}
}

class Client{

	public static void main(String[] args){
		
		Outer.Inner obj = new Outer.Inner();

		obj.m2();
	}
}
