/*Acessing the static variables in inner classes*/


class Outer{
	
	int x = 10;
	static int y = 20;

	class Inner extends Outer{
		
		int a = 40;
		final static int b = 30;

		
		void fun(){
		        System.out.println(y);		
			System.out.println("In Inner fun");
			gun();
		}
	}

	static void gun(){
		
		System.out.println("In Gun Outer");
	}

}



class Client{

	public static void main(String[] args){
		
		Outer obj = new Outer();
		//System.out.println(obj.Inner.b);
		//      |
		//     \_/
		/*program06.java:12: error: Illegal static declaration in inner class Outer.Inner
                static int b = 30;
                           ^
  		modifier 'static' is only allowed in constant variable declarations
		program06.java:27: error: unexpected type
                System.out.println(obj.Inner.b);
                                   ^
  		required: class,package
 		found:    variable
		2 errors*/

		Outer.Inner obj1 = obj.new Inner();
		obj1.fun();
		
	}
}

