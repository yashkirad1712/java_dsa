class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In Inner-Fun");
		}
	}

	void m2(){
		
		System.out.println("In Outer-Fun");
	}
}

class Client{

	public static void main(String[] args){
		
		Outer.Inner obj = new Outer().new Inner();
		Outer obj1 = new Outer();

		obj.m1();
		obj1.m2();

		obj.m1();
		/*We can access the methods of the outer class on the object of the inner class*/
	}
}
