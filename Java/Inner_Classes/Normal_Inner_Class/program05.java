/*Using this$0 and this*/

class Outer{
	
	Outer(){
		System.out.println("In Outer Constructor");
		System.out.println("The value of Outer this is :" + this);
	} 

	int y = 20;

	class Inner{
		
		Inner(){
			
			System.out.println("In Inner Constructor");
			System.out.println("Value of Inner this is: " + this);
			System.out.println("The Value of X variable in Inner class is : " + this.x);
		}

		int x = 10;

		void fun1(){
			
			System.out.println("In Inner - Fun1");
			System.out.println("The value of this$0 is :" + Outer.this);
			System.out.println("The value of Y variable of Outer class is :" + y);
			System.out.println("Calling the outer fun2 method");
			fun2();
		}
	}

	void fun2(){
		
		System.out.println("In Outer - fun2");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer().new Inner();
		obj.fun1();
	}
}
