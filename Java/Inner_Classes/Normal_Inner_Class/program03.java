/*Using static classes in Inner class*/

class Outer{
	
	class Inner{
		
		//Static here is not a access specifier it is a Modifier
		static void m1(){
			
			System.out.println("In Inner Static m1");
		}

		void m2(){
			
			System.out.println("In Inner Non-Static m2");
		}
	}

	void m3(){
		
		System.out.println("In Outer Non-Staic m3");
	}

	static void m4(){
		System.out.println("In Staic Outer m4");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();

		Outer.Inner obj2 = obj1.new Inner();
		
		obj1.Inner.m1();
		obj1.m3();
	}
}

/*It is not compiled becuse the inner class itself is considerd as a instance of the outer class and thats why its not accessed by its name*/
