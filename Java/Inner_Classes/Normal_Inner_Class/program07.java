/*Checking access of static variables in 1.8 java 
 *
 * access of static was from 17 and above but not in the previous ones*/
/*program07.java:10: error: illegal start of expression
                        static y = 20;
                        ^
1 error*/


class Outer{
	
	class Inner{
		void m1(){
			int x = 10;
			static y = 20;

			System.out.println("x = " + x + "y = " + y);
		}
	}	

	public static void main(String[] args){
		
		Inner obj = new Outer().new Inner();
		obj.m1();
	}
}
