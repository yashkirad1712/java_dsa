/*Creating an object of the inner class*/


class Outer{
	
	class Inner{
		
		void fun2(){
			
			System.out.println("In Dun2 -  Inneer");
		}
	}

	void fun1(){
		
		System.out.println("In Fun1 - Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		/*Type 1 :
		 	Outer obj = new Outer();

			Outer.Inner obj1 = obj.New Inner();
			//here obj is taking extra 8 bytes so it is not that much preffered
		 */

		//type 2

		Outer.Inner obj = new Outer().new Inner();
		obj.fun2();
	}
}
