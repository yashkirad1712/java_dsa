/*this$0 concept in Normal Inner Classes */

class Outer{

	int x = 10;
	static int y = 20;

	private void m1(){
		
		System.out.println("In Outer m1");
	}

	class Inner{
		
		void m2(){
			System.out.println("In M2 Inner");
			System.out.println("x = " +x);  //outers Instance variable
			System.out.println("y = " +y);  //Outers Static variable
			m1(); //outers Private Methgod
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer().new Inner();

		//Internals of the object
		//Outer$Inner(Outer$Inner this , Outer this$0)
		//this is the parameters send to the constructor of the inner class when we create an object
		//
		//Constructor of inner class is
		//Outer$Inner(Outer)
		//it has already one paramater i.e. Outers refrence and one hiddedn paramter i.e hidden this of itself
		obj.m2();
	}
}
