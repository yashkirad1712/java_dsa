/*In 1.8 version static in the inner class was only written with the help of the final modifier*/


class Outer{

	class Inner{
		
		final static int x = 10;

		void m1(){
			
			System.out.println("In M1 Inner");
			System.out.println("x = " + x);  //accessing the static variable inside the inner calss
			m2();  //calling Outers static method
		}

	}

	static void m2(){
	
		System.out.println("In m2 Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer().new Inner();

		obj.m1();
		//obj.m2(); we cannot call Outers method from Inners Object from 3 class 
	}
}
