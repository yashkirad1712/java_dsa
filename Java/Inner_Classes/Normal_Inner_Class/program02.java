/*Normal Inner class Main class inside the Outer Class*/

class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In Inner-Fun");
		}
	}

	void m2(){
		
		System.out.println("In Outer Fun");
	}

	public static void main(String[] args){
		
		Inner obj = new Outer().new Inner();
		obj.m1();
		//obj.m2();  
		/*When we write the main class inside the outer class then we canot access the methods of the outer class From the object of the inner class*/
	}
}
