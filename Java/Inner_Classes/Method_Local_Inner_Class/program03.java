/*Trying to craete an MEthod Local Inner class with  level 2*/


class Outer{
	
		void m2(){
			{
				class Inner{
				
					void m1(){
				
						System.out.println("IN M1 Inner");
					}
				}
			
				Inner obj1 = new Inner();
				obj1.m1();		
			}



		}
	
}

class Client{
	
	public static void main(String[] args){
		Outer obj = new Outer();
		obj.m2();
	}
}
