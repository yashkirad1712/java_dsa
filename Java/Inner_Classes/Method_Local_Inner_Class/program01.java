/*Method Local Inner Class*/

class Outer {
	
	void m1(){

		class Inner{
			void m1(){
			
				System.out.println("In Inner-Class");
			}
		}

		Inner obj = new Inner();
		obj.m1();	
	}

	void m2(){
		
		System.out.println("In Outer Method");
	}

	public static void main(String[] args){
		
		Outer obj = new Outer();

		obj.m1();
		obj.m2();
	}
}
